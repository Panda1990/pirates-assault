package Pirates_Assault;

import java.util.List;

public class CannonBall extends Object_Game {
	
	private static final long serialVersionUID = -5942971053101319623L;
	int velocity;
	int range;
	Object_Game sender;
	
	public CannonBall (int _velocity, int _range, Object_Game _sender){
		//coordSX = new Pair (_sender.coordSX.x, _sender.coordSX.y);
		/*coordSX.x = _sender.coordSX.x;
		coordSX.y = _sender.coordSX.y;*/
		data = new Pair (5,5);
		range = _range;
		velocity = _velocity;
		sender = _sender;
	}
	
	
	public int fire (int _x, int _y, List<Enemy> enemies) {
		
		if (coordSX == null)
			coordSX = new Pair(_x, _y);
		else{
			coordSX.x = _x;
			coordSX.y = _y;
		}
		double distance = Math.sqrt( Math.pow (_x - sender.coordSX.x, 2) + Math.pow(_y - sender.coordSX.y, 2) );
		if (distance < this.range){
			for (Enemy tmp: enemies)
				if (this.Collide(tmp)){
					int hp = tmp.stats.getHealth_point() - (sender.stats.getCannon_attack() / tmp.stats.cannon_defense + 10);
					tmp.stats.setHealth_point(hp);
					return 0;
				}
			return 2;
		}
		return 4;
	}
	
	@Override
	public boolean fire(Player _player) {
		BooleanRandom rndm = new BooleanRandom();
		
		if (coordSX == null)
			coordSX = new Pair(sender.coordSX.x, sender.coordSX.y);
		else{
			coordSX.x = sender.coordSX.x;
			coordSX.y = sender.coordSX.y;
		}
		
		if (rndm.booleanRandom(500)){
			double distance = Math.sqrt( Math.pow (_player.getPosX() - coordSX.x, 2) + Math.pow(_player.getPosY() - coordSX.y, 2) );
			System.out.println("DISTANCE:" + distance);
			System.out.println("RANGE: " + range);
			if (distance < this.range){
				
				double start = System.currentTimeMillis();
				double time =  distance / velocity;
				
				while (true){
					if (System.currentTimeMillis() - start >=  time){
								_player.stats.setHealth_point(_player.stats.getHealth_point() - (sender.stats.getCannon_attack() / _player.stats.cannon_defense + 10));
								System.out.println("COLPITO");
								return true;
								
					}
				}
			}
		}
		return false;
			
	}
	
	@Override
	public boolean Collide(ICollidable tmp) {
		if (tmp instanceof Enemy && tmp != sender){
			Enemy ship = (Enemy) tmp;
			int AX,AY,BX,BY;
			BX = ship.coordSX.x + ship.data.x;
			BY = ship.coordSX.y + ship.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		else if (tmp instanceof Player && tmp != sender){
			Player ship = (Player)tmp;
			int AX,AY,BX,BY;
			BX = ship.coordSX.x + ship.data.x;
			BY = ship.coordSX.y + ship.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		return false;
	}


	@Override
	public void move(long _delay) {
		return;
		
	}
}
