package Pirates_Assault;

import java.io.Serializable;
import java.util.List;

public interface IFireAble extends Serializable {
	public int fire (int _x, int _y, List<Enemy> _enemies);
	public boolean fire (Player _player);
}
