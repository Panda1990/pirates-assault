package Pirates_Assault;

public abstract class PowerUP implements IMovable {
	
	private static final long serialVersionUID = -6323337125508073492L;
	long _currentTime;
	long beforeTime;
	long delay;
	int velocity;
	boolean animation;
	Pair coordSX;
	int player_direction;
	Pair data_player;
	boolean piccola;

	public void setPlayerDirection(int index)
	{
		player_direction = index;
	}
	
	public void setDataPlayer(Pair c)
	{
		data_player = c;
	}
	
	public PowerUP getPowerUp()
	{
		return this;
	}
	
	public void modifyStats(Stats stat) {}
	
	public void returnToStandardStats(Stats stat) {}

	public int getPosY ()
	{
		return this.coordSX.y;
	}
	
	public int getPosX ()
	{
		return this.coordSX.x;
	}
	
	public void setPosY(int index)
	{
		this.coordSX.y = index;
	}
	
	public void setPosX(int index)
	{
		this.coordSX.x = index;
	}
	
	public void setCoordSX(Pair c)
	{
		this.coordSX = c;
	}
	
	public void setVelocity(int index)
	{
		velocity = index;
	}
	
	public void setBeforeTime(long index)
	{
		beforeTime = index;
	}
	
	public void setPiccola(boolean b)
	{
		piccola = b;
	}
}
