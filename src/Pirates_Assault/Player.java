package Pirates_Assault;

import java.awt.event.KeyEvent;
import java.util.List;

public class Player extends Object_Game {
	
	private static final long serialVersionUID = 7715149559165310973L;
	//dx, sx, up, down;
	private boolean animation;
	boolean power; // se la nave ha power up... non so se serve
	PowerUP power_up; // il power up che ha al momento la nave
	private int sprite; // decide lo sprite che devo usare
	long beforeTime;
	long time_to_power;
	long delay_to_power;
	private int delay;
	private int ammo;
	int dobloni;
	boolean barchetta;
	boolean both;
	
	boolean [] possible_move;
	
	VisibleScreen screen;
	
	public Player (Pair _coordSX, Pair _data, Stats _stats){
		
		coordSX = _coordSX;
		data = _data;
		stats = _stats;
		power = false;
		sprite = 2;
		animation = true;
		delay = 600;
		beforeTime = System.currentTimeMillis();
		delay_to_power = 10000;
		ammo = 10;
		possible_move = new boolean [4];
		dobloni = 0;
		barchetta = true;
		both = false;
	}
	
	public Player (Player _player){
		coordSX = new Pair (_player.coordSX.x, _player.coordSX.y);
		data = new Pair (_player.data.x, _player.data.y);
		stats = new Stats (new Cannon(_player.stats.cannon.beforeFire, _player.stats.cannon.recharge, _player.stats.cannon.cannonBall), _player.stats.health_point, _player.stats.cannon_attack, _player.stats.physic_attack, _player.stats.cannon_defense, _player.stats.physic_defense, _player.stats.velocity);
		power = false;
		sprite = 2;
		animation = true;
		delay = 600;
		beforeTime = System.currentTimeMillis();
		delay_to_power = 10000;
		ammo = 10;
		possible_move = new boolean [4];
		dobloni = _player.dobloni;
		barchetta = _player.barchetta;
		both = _player.both;
	}
	
	public void reset (){
		for (int i = 0; i < possible_move.length; i++) {
			possible_move [i] = true;
		}
	}
	
	public void check_position (IModGame mod){
		this.reset ();
		if (mod instanceof Game){
			Game game = (Game) mod;
			for (int i = 0; i < game.islands.length; i++){
				int coll = coordSX.x + this.data.x + 5;
				int AX,AY,BX,BY;
				
				BX = game.islands[i].coordSX.x + game.islands[i].data.x;
				BY = game.islands[i].coordSX.y + game.islands[i].data.y;
				AX = coll + 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coll) || (AY < game.islands[i].coordSX.y) || (BY < coordSX.y))){
					possible_move [0] = false;
					this.coordSX.x = game.islands[i].getCoordSX().x - this.data.x - 5;
					game.win [i] = true;
				}
				
				coll = coordSX.x - 5;
				AX = coll - 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coll) || (AY < game.islands[i].coordSX.y) || (BY < coordSX.y))){
					possible_move [1] = false;
					this.coordSX.x = game.islands[i].getCoordSX().x + game.islands[i].getData().x + 5;
					game.win [i] = true;
				}
				
				coll = coordSX.y - 30;
				AX = coordSX.x + data.x;
				AY = coll - 5;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coordSX.x) || (AY < game.islands[i].coordSX.y) || (BY < coll))){
					possible_move [2] = false;
					coordSX.y = game.islands[i].getCoordSX().y + game.islands[i].getData().y + 5;
					game.win [i] = true;
				}
				
				coll = coordSX.y + data.y + 5;
				AX = coordSX.x + data.x;
				AY = coll;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coordSX.x) || (AY < game.islands[i].coordSX.y) || (BY < coll))){
					possible_move [3] = false;
					this.coordSX.y = game.islands[i].getCoordSX().y - this.data.y - 30;
					game.win [i] = true;
				}
			}
			
			for (Enemy tmp : game.enemies){
				int coll = coordSX.x + this.data.x + 5;
				int AX,AY,BX,BY;
				
				BX = tmp.coordSX.x + tmp.data.x;
				BY = tmp.coordSX.y + tmp.data.y;
				AX = coll + 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < tmp.coordSX.x) || (BX < coll) || (AY < tmp.coordSX.y) || (BY < coordSX.y))){
					possible_move [0] = false;
					this.coordSX.x = tmp.getCoordSX().x - this.data.x - 5;
				}
				
				coll = coordSX.x - 5;
				AX = coll - 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < tmp.coordSX.x) || (BX < coll) || (AY < tmp.coordSX.y) || (BY < coordSX.y))){
					possible_move [1] = false;
					this.coordSX.x = tmp.getCoordSX().x + tmp.getData().x + 5;
				}
				
				coll = coordSX.y - 30;
				AX = coordSX.x + data.x;
				AY = coll - 5;
				if  (!( (AX < tmp.coordSX.x) || (BX < coordSX.x) || (AY < tmp.coordSX.y) || (BY < coll))){
					possible_move [2] = false;
					coordSX.y = tmp.getCoordSX().y + tmp.getData().y + 5;
				}
				
				coll = coordSX.y + data.y + 5;
				AX = coordSX.x + data.x;
				AY = coll;
				if  (!( (AX < tmp.coordSX.x) || (BX < coordSX.x) || (AY < tmp.coordSX.y) || (BY < coll))){
					possible_move [3] = false;
					this.coordSX.y = tmp.getCoordSX().y - this.data.y - 30;
				}
			}
		}
		
		else if (mod instanceof SurvivalMod){
			SurvivalMod game = (SurvivalMod) mod;
			for (int i = 0; i < game.islands.length; i++){
				int coll = coordSX.x + this.data.x + 5;
				int AX,AY,BX,BY;
				
				BX = game.islands[i].coordSX.x + game.islands[i].data.x;
				BY = game.islands[i].coordSX.y + game.islands[i].data.y;
				AX = coll + 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coll) || (AY < game.islands[i].coordSX.y) || (BY < coordSX.y))){
					possible_move [0] = false;
					this.coordSX.x = game.islands[i].getCoordSX().x - this.data.x - 5;
				}
				
				coll = coordSX.x - 5;
				AX = coll - 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coll) || (AY < game.islands[i].coordSX.y) || (BY < coordSX.y))){
					possible_move [1] = false;
					this.coordSX.x = game.islands[i].getCoordSX().x + game.islands[i].getData().x + 5;
				}
				
				coll = coordSX.y - 30;
				AX = coordSX.x + data.x;
				AY = coll - 5;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coordSX.x) || (AY < game.islands[i].coordSX.y) || (BY < coll))){
					possible_move [2] = false;
					coordSX.y = game.islands[i].getCoordSX().y + game.islands[i].getData().y + 5;
				}
				
				coll = coordSX.y + data.y + 5;
				AX = coordSX.x + data.x;
				AY = coll;
				if  (!( (AX < game.islands[i].coordSX.x) || (BX < coordSX.x) || (AY < game.islands[i].coordSX.y) || (BY < coll))){
					possible_move [3] = false;
					this.coordSX.y = game.islands[i].getCoordSX().y - this.data.y - 30;
				}
			}
			
			for (Enemy tmp : game.enemies){
				int coll = coordSX.x + this.data.x + 5;
				int AX,AY,BX,BY;
				
				BX = tmp.coordSX.x + tmp.data.x;
				BY = tmp.coordSX.y + tmp.data.y;
				AX = coll + 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < tmp.coordSX.x) || (BX < coll) || (AY < tmp.coordSX.y) || (BY < coordSX.y))){
					possible_move [0] = false;
					this.coordSX.x = tmp.getCoordSX().x - this.data.x - 5;
				}
				
				coll = coordSX.x - 5;
				AX = coll - 1;
				AY = coordSX.y + data.y;
				if  (!( (AX < tmp.coordSX.x) || (BX < coll) || (AY < tmp.coordSX.y) || (BY < coordSX.y))){
					possible_move [1] = false;
					this.coordSX.x = tmp.getCoordSX().x + tmp.getData().x + 5;
				}
				
				coll = coordSX.y - 30;
				AX = coordSX.x + data.x;
				AY = coll - 5;
				if  (!( (AX < tmp.coordSX.x) || (BX < coordSX.x) || (AY < tmp.coordSX.y) || (BY < coll))){
					possible_move [2] = false;
					coordSX.y = tmp.getCoordSX().y + tmp.getData().y + 5;
				}
				
				coll = coordSX.y + data.y + 5;
				AX = coordSX.x + data.x;
				AY = coll;
				if  (!( (AX < tmp.coordSX.x) || (BX < coordSX.x) || (AY < tmp.coordSX.y) || (BY < coll))){
					possible_move [3] = false;
					this.coordSX.y = tmp.getCoordSX().y - this.data.y - 30;
				}
			}
		}
}
	
	
	public void setScreen(VisibleScreen screen) {
		this.screen = screen;
	}



	@Override
	public boolean Collide(ICollidable tmp) {
		
		if (tmp instanceof CannonBall){
			CannonBall ball = (CannonBall) tmp;
			if (ball.sender instanceof Enemy){
				int AX,AY,BX,BY;
				BX = ball.coordSX.x + ball.data.x;
				BY = ball.coordSX.y + ball.data.y;
				AX = coordSX.x + data.x;
				AY = coordSX.y + data.y;
				return  !( (AX < ball.coordSX.x) || (BX < coordSX.x) || (AY < ball.coordSX.y) || (BY < coordSX.y) );
			}
		}
		
		else if (tmp instanceof Enemy){
			Enemy ship = (Enemy) tmp;
			int AX,AY,BX,BY;
			BX = ship.coordSX.x + ship.data.x;
			BY = ship.coordSX.y + ship.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		
		else if (tmp instanceof Island){
			Island island = (Island) tmp;
			int AX,AY,BX,BY;
			BX = island.coordSX.x + island.data.x;
			BY = island.coordSX.y + island.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < island.coordSX.x) || (BX < coordSX.x) || (AY < island.coordSX.y) || (BY < coordSX.y) );
		}
		return false;
	}
	
	public int getImagePlayer ()
	{	
		if (barchetta){
			if (sprite == 2)
			{
				if (animation)
					return 0; //upper_1;
				else
					return 1; // upper_2;
			}
			if (sprite == 0)
			{
				return 2 ;// right;
			}
			if (sprite == 1)
			{
				return 3; //left;
			}
			else 
			{
				return 4; // down;
			}
				
		}
		else {
			if (sprite == 2){
				if (animation)
					return 5;
				else
					return 6;
			}
			if (sprite == 0){
				if (animation)
					return 7;
				else
					return 8;
			}
			if (sprite == 1){
				if (animation)
					return 9;
				else
					return 10;
			}
			else{
				if (animation)
					return 11;
				else
					return 12;
			}
		}
	}
	
	public int getPosY ()
	{
		return this.coordSX.y;
	}
	
	public int getPosX ()
	{
		return this.coordSX.x;
	}
	
	public void invertUno(){
		if (data.x == 49){
			data.x = 100;
			data.y = 100;
		}
		else{
			data.x = 49;
			data.y = 89;
		}
	}
	
	public void invertDue(){
		if (data.x == 99){
			data.x = 178;
			data.y = 152;
		}
		else{
			data.x = 99;
			data.y = 190;
		}
			
	}
	
	public void keyPressed(KeyEvent e)
	{
		int key = e.getKeyCode();
		
		if (key == KeyEvent.VK_UP)
		{
			if ((sprite != 2 || sprite != 3) && barchetta){
				invertUno ();
			}
			else if ((sprite != 2 || sprite != 3) && !barchetta){
				invertDue();
			}
			this.move(2);
			sprite = 2;
			if (power)
			{
				power_up.setPlayerDirection(2);
				power_up.setPiccola(barchetta);
			}
		}
		
		if (key == KeyEvent.VK_RIGHT)
		{
			if ((sprite != 1 || sprite != 0) && barchetta){
				invertUno ();
			}
			else if ((sprite != 1 || sprite != 0) && !barchetta){
				invertDue();
			}
			this.move(0);
			sprite = 0;
			if (power)
			{
				power_up.setPlayerDirection(0);
				power_up.setPiccola(barchetta);
			}
		}
		if (key == KeyEvent.VK_LEFT)
		{
			if ((sprite != 1 || sprite != 0) && barchetta)
				invertUno ();
			else if ((sprite != 1 || sprite != 0) && !barchetta)
				invertDue ();
			this.move(1);
			sprite = 1;
			if (power)
			{
				power_up.setPlayerDirection(1);
				power_up.setPiccola(barchetta);
			}
		}
		
		if (key == KeyEvent.VK_DOWN)
		{
			if ((sprite != 2 || sprite != 3) && barchetta)
				invertUno();
			else if ((sprite != 2 || sprite != 3) && !barchetta)
				invertDue();
			this.move(3);
			sprite = 3;
			if (power)
			{
				power_up.setPlayerDirection(3);
				power_up.setPiccola(barchetta);
			}
		}
		
	}
	
	public void keyReleased(KeyEvent e)
	{
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_LEFT || key == KeyEvent.VK_RIGHT)
		{
	
		}
	}

	public void move (int direction){
		switch (direction){
		case 0: // go right;
			if ((this.coordSX.x + data.x < dimensions.x - this.data.x) && possible_move [0])
				this.coordSX.x += this.stats.velocity;
			break;
		case 1: // go left;
			if (this.coordSX.x > 0 && possible_move [1])
				this.coordSX.x -= this.stats.velocity;
			break;
		case 2: // go up;
			if (this.coordSX.y > 0 && possible_move [2])
				this.coordSX.y -= this.stats.velocity;
			break;
		case 3: // go down
			if ((this.coordSX.y + this.data.y < dimensions.y - this.data.y) && possible_move [3])
				this.coordSX.y += this.stats.velocity;
			break;
		default:
			break;
		}
		
	}

	@Override
	public void move(long _currentTime)
	{
		if (beforeTime + delay <= _currentTime)
		{
			animation = !animation;
			beforeTime = _currentTime;
		}
	}
	
	// 0 colpito, 1 Senza munizioni, 2 mancato,  3 ricaricare, 4 fuori range       
	public int fire(int _x, int _y, List<Enemy> enemies) {
		if (ammo <= 0)
			return 1;
		int ret = 3;
		if (System.currentTimeMillis() >= this.stats.cannon.beforeFire + this.stats.cannon.recharge){
			this.stats.cannon.beforeFire = System.currentTimeMillis();
			this.ammo--;
			ret = this.stats.cannon.cannonBall.fire(_x, _y, enemies);
		}
		return ret;
	}
	
	public void setAmmo(int index)
	{
		ammo += index;
	}
	
	public void setHealthPoint (int index)
	{
		this.stats.health_point += index;
	}
	
	public void checkBonus(PowerUP bonus) 
	{
		if (power)
			return;
		
		power_up = bonus;
		if (power_up instanceof AmmoPerTwo)
		{
			time_to_power = System.currentTimeMillis();
			ammo = ((AmmoPerTwo) power_up).setAmmo(ammo);
			power_up.setCoordSX(this.coordSX);
			power_up.setVelocity(this.stats.velocity);
			power = true;
			return;
		}
		if (!power)
		{
			time_to_power = System.currentTimeMillis();
			power = true;
			power_up.modifyStats(stats);
			power_up.setCoordSX(this.coordSX);
			power_up.setVelocity(this.stats.velocity);
			power_up.setDataPlayer(this.data);
			return;
		}
	}
	
	public void durationPowerUp(long _currentTime)
	{
		if (time_to_power+delay_to_power <= _currentTime && power)
		{
			power = false;
			power_up.returnToStandardStats(stats);
		}
		else
			power_up.move(_currentTime);
	}
	
	public boolean getPower()
	{
		return this.power;
	}
	
	public PowerUP getPowerUp ()
	{
		return this.power_up;
	}

	public int getAmmo() {
		return ammo;
	}

	public Player getPlayer (){
		return this;
	}
	
	public void setBarchetta (boolean _barchetta){
		barchetta = _barchetta;
	}
	
	public boolean getBarchetta (){
		return barchetta;
	}

	public int getDobloni() {
		return dobloni;
	}

	public void setDobloni(int dobloni) {
		this.dobloni += dobloni;
	}

	public boolean isBoth() {
		return both;
	}

	public void setBoth(boolean both) {
		this.both = both;
	}
	
	public int getHealthPoint()
	{
		return this.stats.health_point;
	}
}
