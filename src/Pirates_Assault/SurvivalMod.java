package Pirates_Assault;

public class SurvivalMod extends Game {
	
	private static final long serialVersionUID = -9009566535841061688L;
	long beforeSpawn;
	long refreshSpawn;
	
	public SurvivalMod(Player _player, int _intelligent) {
		super(_player, _intelligent);

		beforeSpawn = System.currentTimeMillis();
		refreshSpawn = 50000;
		spawn = 15;
	}
	
	@Override
	public void refresh() {
		long currentTime = System.currentTimeMillis();
		if (beforeSpawn + refreshSpawn >= currentTime){
			spawn += 5;
			beforeSpawn = currentTime;
		}
			super.refresh();
	}

}
