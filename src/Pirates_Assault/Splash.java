package Pirates_Assault;

import java.io.Serializable;

public class Splash implements Serializable{
	private static final long serialVersionUID = 4426947494241157671L;
	
	Pair coord;
	int index;
	long currentTime;
	long delay;
	
	public Splash (int x, int y){
		coord = new Pair (x, y);
		index = 0;
		currentTime = System.currentTimeMillis();
		delay = 150;
	}
	
	public Pair getCoord (){
		return coord;
	}
	
	public void updateIndex (){
		if (currentTime + delay < System.currentTimeMillis()){
			index++;
			currentTime = System.currentTimeMillis();
	}
	}
	
	public int getIndex (){
		return index;
	}
	public void move_screen (int _direction, int _velocity){
		switch (_direction){
		case 0: // go right;
			coord.x = coord.x - _velocity;
			break;
		case 1: // go left;
			coord.x = coord.x + _velocity;
			break;
		case 2: // go up;
			coord.y = coord.y + _velocity;
			break;
		case 3: // go down;
			coord.y = coord.y - _velocity;
			break;
		default:
			break;
		}
	
	}
}