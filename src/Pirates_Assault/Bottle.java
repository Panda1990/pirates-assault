package Pirates_Assault;

import java.util.Random;

public class Bottle extends ScenicContent {

	private static final long serialVersionUID = 1168029445750686928L;
	//POWER UP
	// 0 = ammo*2; 1 = velocit�*2; 2 = scudo; 
	PowerUP power_up;
	
	public Bottle(boolean tmp)
	{
		rndm = new Random ();
		left_direction = tmp;
		data = new Pair (20, 30);
		
		if (tmp){
			direction = 1;
			coordSX = new Pair (dimensions.x, rndm.nextInt(dimensions.y));
		}
		else{
			direction = 0;
			coordSX = new Pair (0, rndm.nextInt(dimensions.y));
		}
		beforeTime = System.currentTimeMillis();
		delay = 700;
		animation = true;
		velocity = 4;
		bonus = rndm.nextInt(3);
	}
	
	public int getImage ()
	{
		if (animation)
			return 0;
		else
			return 1;
	}
	

	@Override
	public boolean Collide(ICollidable tmp) 
	{
		Player ship = (Player) tmp;
		int AX,AY,BX,BY;
		BX = ship.coordSX.x + ship.data.x;
		BY = ship.coordSX.y + ship.data.y;
		AX = coordSX.x;
		AY = coordSX.y;
		return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
	}

	@Override
	public void move(long _currentTime) 
	{
		super.move(_currentTime, velocity);
	}
	
	public PowerUP getBonus()
	{
		if (bonus == 0)
			return (new AmmoPerTwo());
		if (bonus == 1)
			return (new SuperSpeed());
		else 
			return (new SuperShield());
	}
}
