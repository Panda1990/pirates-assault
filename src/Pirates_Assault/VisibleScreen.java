package Pirates_Assault;

import java.io.Serializable;

import application.Info;

public class VisibleScreen implements Serializable{

	private static final long serialVersionUID = 7290406249963185715L;
	
	Pair A;
	Pair B;
	Pair C;
	Pair D;
	
	Pair matrix_a;
	Pair matrix_b;
	Pair matrix_c;
	Pair matrix_d;
	
	int velocity;
	
	static final public Pair dimensions = Info.Information.dimensions;
			
	public VisibleScreen (){
		A = new Pair (0, 0);
		B = new Pair (dimensions.x - 1, 0);
		C = new Pair (0, dimensions.y - 1);
		D = new Pair (dimensions.x - 1, dimensions.y -1);
		
		matrix_a = new Pair (0,0);
		matrix_b = new Pair (0,0);
		matrix_c = new Pair (0,0);
		matrix_d = new Pair (0,0);
		System.out.println("INIZIA");
		System.out.println("Matrix_a: " + matrix_a.x + "  " + matrix_a.y);
		System.out.println("Matrix_b: " + matrix_b.x + "  " + matrix_b.y);
		System.out.println("Matrix_c: " + matrix_c.x + "  " + matrix_c.y);
		System.out.println("Matrix_d: " + matrix_d.x + "  " + matrix_d.y);
		
		velocity = 3;
		
	}
	
	public boolean goRight (){
		if (B.x < dimensions.x * 5)
			return true;
		return false;
	}
	
	public boolean goLeft (){
		if (A.x > 0)
			return true;
		return false;
	}
	
	public boolean goUp (){
		if (A.y > 0)
			return true;
		return false;
	}
	
	public boolean goDown (){
		if (C.y < dimensions.y * 5)
			return true;
		return false;
	}
	
	public Pair getA() {
		return A;
	}

	public Pair getB() {
		return B;
	}

	public Pair getC() {
		return C;
	}

	public Pair getD() {
		return D;
	}

	public Pair getMatrix_a() {
		return matrix_a;
	}

	public Pair getMatrix_b() {
		return matrix_b;
	}

	public Pair getMatrix_c() {
		return matrix_c;
	}

	public Pair getMatrix_d() {
		return matrix_d;
	}

	
	
	public void move (int direction){
		switch (direction){
		case 0: // RIGHT
			if (B.x <= dimensions.x * 5){
				A.setX(A.x + velocity);
				B.setX(B.x + velocity);
				C.setX(C.x + velocity);
				D.setX(D.x + velocity);
				}
			break;
		case 1: // LEFT
			if (A.x >= 3){
				A.setX(A.x - velocity);
				B.setX(B.x - velocity);
				C.setX(C.x - velocity);
				D.setX(D.x - velocity);
			}
			break;
		case 2: // UP
			if (A.y >= 3){
				A.setY(A.y - velocity);
				B.setY(B.y - velocity);
				C.setY(C.y - velocity);
				D.setY(D.y - velocity);
			}
			break;
		case 3: // DOWN
			if (C.y <= dimensions.y * 5){
				A.setY(A.y + velocity);
				B.setY(B.y + velocity);
				C.setY(C.y + velocity);
				D.setY(D.y + velocity);
			}
			break;
		default:
			break;
		}
		matrix_a.x = (int) (A.x / dimensions.x);
		matrix_a.y = (int) (A.y / dimensions.y);
		
		matrix_b.x = (int) (B.x / dimensions.x);
		matrix_b.y = (int) (B.y / dimensions.y);
		
		matrix_c.x = (int) (C.x / dimensions.x);
		matrix_c.y = (int) (C.y / dimensions.y);
		
		matrix_d.x = (int) (D.x / dimensions.x);
		matrix_d.y = (int) (D.y / dimensions.y);
		
		/*System.out.println("CHANGE");
		System.out.println("Matrix_a: " + matrix_a.x + "  " + matrix_a.y);
		System.out.println("Matrix_b: " + matrix_b.x + "  " + matrix_b.y);
		System.out.println("Matrix_c: " + matrix_c.x + "  " + matrix_c.y);
		System.out.println("Matrix_d: " + matrix_d.x + "  " + matrix_d.y);
		

		System.out.println("A: " + A.x + "  " + A.y);
		System.out.println("B: " + B.x + "  " + B.y);
		System.out.println("C: " + C.x + "  " + C.y);
		System.out.println("D: " + D.x + "  " + D.y);*/
	}
}
