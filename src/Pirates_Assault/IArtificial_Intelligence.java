package Pirates_Assault;

import java.io.Serializable;

public interface IArtificial_Intelligence extends Serializable {
	
	public void move (long _currentTime, Player hero);
	public void switch_direction ();
}
