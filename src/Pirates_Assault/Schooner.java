package Pirates_Assault;

import java.util.Random;

public class Schooner extends Enemy {
	
	// 0 right, 1 left, 2 up, 3 down
	
	private static final long serialVersionUID = -3890642317222890241L;

	public Schooner (boolean tmp){
		dobloni = 1;
		rndm = new Random ();
		stats = new Stats (new Cannon(10,  10,  new CannonBall(30, 300,  this)), 50, 50, 20, 8, 5, 5);
		direction = rndm.nextInt(4);
		
		switch (direction){
		case 0:
			sprite = 0; 
			coordSX = new Pair(-75, dimensions.y); 
			data = new Pair (75, 67); 
			break;
		
		case 1:
			sprite = 1;
			coordSX = new Pair(dimensions.x, dimensions.y); 
			data = new Pair (75, 67); 
			break;
		
		case 2: 
			sprite = 2;
			coordSX = new Pair(dimensions.x, -50); 
			data = new Pair (50, 96); 
			break;
		
		case 3:
			sprite = 3; 
			coordSX = new Pair(dimensions.x, dimensions.y); 
			data = new Pair (50, 89); 
			break;
		}
		beforeTime = System.currentTimeMillis();
		delay = 92;
		animation = true;
	}
	
	public int getImage() {
		
		if (sprite == 0)
		{
			data.x = 75;
			data.y = 67;
			if (animation)
				return 0;//right_1;
			else
				return 1;//right_2;
		}
		
		else if (sprite == 1)
		{
			data.x = 75;
			data.y = 67;
			if (animation)
				return 2;//left_1;
			else
				return 3;//left_2;
		}
		
		else if (sprite == 2)
		{
			data.x = 35;
			data.y = 89;
			if (animation)
				return 4;//up_1;
			else
				return 5;//up_2;
		}
		
		else
		{
			data.x = 28;
			data.y = 96;
			if (animation)
				return 6;//down_1;
			else
				return 7;//down_2;
		}
	}
}
