package Pirates_Assault;

import java.io.IOException;
import java.io.Serializable;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class Detect implements Serializable{
	
	private static final long serialVersionUID = -675559415194628668L;
	
	String path; // ///home/mario/workspace/...
	public List<Class> fileInJar;
	
	public Detect (String _path){
		path = new String ("jar:file:" + _path + "/NewIntelligence.jar" + "!/");
		fileInJar = new ArrayList <Class>();
	}
	
	public List<Class> search (){
		JarFile jarfile;
		try {
			jarfile = new JarFile (path.substring(10, path.length()-2));
			Enumeration <JarEntry> e = jarfile.entries();
			
			URL[] urls = { new URL(path) };
			URLClassLoader cl = URLClassLoader.newInstance(urls);
			
			while (e.hasMoreElements()) {
				JarEntry je = e.nextElement();
				if(je.isDirectory() || !je.getName().endsWith(".class")){
					continue;
				}
				// -6 because of .class
			String className = je.getName().substring(0,je.getName().length()-6);
			className = className.replace('/', '.');
			
			if(className.contains("Intelligence")){
				Class c = cl.loadClass(className);
				fileInJar.add(c);
					}
			
				}
			
			} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
			}catch (IOException e1) {
			e1.printStackTrace();
			}
			return fileInJar;
		}
}
