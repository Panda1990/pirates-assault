package Pirates_Assault;

public class SuperSpeed extends PowerUP {
	
	private static final long serialVersionUID = -5511068861257949635L;
	int velocity;
	
	public SuperSpeed ()
	{
		velocity = 10;
		delay = 600;
	}

	@Override
	public void modifyStats(Stats stat) 
	{
		stat.velocity += velocity;
	}

	@Override
	public void returnToStandardStats(Stats stat) 
	{
		stat.velocity -= velocity;
	}

	@Override
	public void move(long _delay) 
	{
		// TODO Auto-generated method stub
	}

	public int getImage ()
	{
		return player_direction;
	}
	
	@Override
	public int getPosY() 
	{
		if (piccola)
		{
			if (player_direction == 0)
				return super.getPosY()+50;
			if (player_direction == 1)
				return super.getPosY()+50;
			if (player_direction == 2)
				return super.getPosY()+90;
			else
				return super.getPosY()-90;
		}
		else
		{
			if (player_direction == 0)
				return super.getPosY()+90;
			if (player_direction == 1)
				return super.getPosY()+90;
			if (player_direction == 2)
				return super.getPosY()+185;
			else
				return super.getPosY()-85;
		}
				
	}

	@Override
	public int getPosX() 
	{
		if (piccola)
		{
			if (player_direction == 0)
				return super.getPosX()-100;
			if (player_direction == 1)
				return super.getPosX()+100;
			if (player_direction == 2)
				return super.getPosX()-5;
			else
				return super.getPosX()-5;
		}
		else
		{
			if (player_direction == 0)
				return super.getPosX()-98;
			if (player_direction == 1)
				return super.getPosX()+175;
			if (player_direction == 2)
				return super.getPosX()+13;
			else
				return super.getPosX()+10;
		}
	}
	
	
}
