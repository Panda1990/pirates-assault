package Pirates_Assault;

public class Intelligence implements IArtificial_Intelligence{

	private static final long serialVersionUID = -7617626975564372676L;
	Enemy enemy;
	
	Intelligence (){ }
	
	Intelligence (Enemy _enemy){
		enemy = _enemy;
	}
	
	public Enemy getEnemy() {
		return enemy;
	}

	public void setEnemy(Enemy enemy) {
		this.enemy = enemy;
	}

	public void move (long _currentTime, Player hero) 
	{
		if (enemy.beforeTime + enemy.delay <= _currentTime)
		{	
			enemy.animation = !enemy.animation;
			
			switch (enemy.direction){
			case 0: // go right;
				enemy.coordSX.x = enemy.coordSX.x + enemy.stats.getVelocity();
				break;
			case 1: // go left;
				enemy.coordSX.x = enemy.coordSX.x - enemy.stats.getVelocity();
				break;
			case 2: // go up;
				enemy.coordSX.y = enemy.coordSX.y + enemy.stats.getVelocity();
				break;
			case 3: // go down;
				enemy.coordSX.y = enemy.coordSX.y - enemy.stats.getVelocity();
				break;
			default:
				break;
			}
			enemy.beforeTime = _currentTime;
		}
		return;
	}
	
	public void switch_direction (){
		int newDirection = enemy.rndm.nextInt(2);
		if (enemy.direction == 0 || enemy.direction == 1){
			if (enemy.direction == 0)
				enemy.coordSX.x -= this.enemy.stats.getVelocity();
			else
				enemy.coordSX.x += this.enemy.stats.getVelocity();
			enemy.direction = newDirection + 2;
			enemy.sprite = enemy.direction;
		}
		else{
			if (enemy.direction == 3)
				enemy.coordSX.y -= this.enemy.stats.getVelocity();
			else
				enemy.coordSX.y += this.enemy.stats.getVelocity();
			enemy.direction = newDirection;
			enemy.sprite = enemy.direction;
		}
	}
	

}
