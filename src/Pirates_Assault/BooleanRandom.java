package Pirates_Assault;

import java.io.Serializable;
import java.util.Random;

public class BooleanRandom implements Serializable{
	
	private static final long serialVersionUID = 1539172776251642356L;

	public boolean booleanRandom(int range)
	{
		Random rnd = new Random();
		if(rnd.nextInt(range) == 0)
			return true;
		else
			return false;
	}
	

}
