package Pirates_Assault;

import java.io.Serializable;

public class AnimationFire implements Serializable {
	
	private static final long serialVersionUID = 6245981858667433472L;
	
	long before;
	long delay;
	boolean number;
	
	public AnimationFire(boolean _number){
		number = _number;
		before = System.currentTimeMillis();
		delay = 400;
	}

	public long getBefore() {
		return before;
	}

	public void setBefore(long before) {
		this.before = before;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public boolean isNumber() {
		return number;
	}

	public void setNumber(boolean number) {
		this.number = number;
	}
	
}

