package Pirates_Assault;

public class SuperShield extends PowerUP {
	
	private static final long serialVersionUID = -5174080047763509118L;
	int defense;
	
	public SuperShield ()
	{
		defense =50;
		delay = 600;
		
	}

	@Override
	public PowerUP getPowerUp() 
	{
		return this;
	}
	
	
	public void modifyStats(Stats stat) 
	{
		stat.physic_defense += defense;
	}

	@Override
	public void returnToStandardStats(Stats stat) 
	{
		stat.physic_defense -= defense;
	}

	
	
	@Override
	public int getPosY() 
	{
		if (piccola)
		{
			return this.coordSX.y-75;
		}
		else
			return this.coordSX.y-165;
	}

	@Override
	public int getPosX() 
	{
		if (piccola)
		{
			if (player_direction == 1 || player_direction == 0)
				return this.coordSX.x-75;
			else
				return this.coordSX.x-110;
		}
		else
			if (player_direction == 1 || player_direction == 0)
				return this.coordSX.x-170;
			else
				return this.coordSX.x-230;
	}	
	
	@Override
	public void move(long _delay) {
		
	}
}