package Pirates_Assault;

import java.awt.Toolkit;
import java.util.Random;

public class Barrel extends ScenicContent{

	private static final long serialVersionUID = -344975739169289725L;

	public Barrel (boolean tmp)
	{
		rndm = new Random ();
		left_direction = tmp;
		
		if (tmp)
		{
			direction = 1;
			coordSX = new Pair ((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth(), rndm.nextInt((int) Toolkit.getDefaultToolkit().getScreenSize().getHeight()));
		}
		else
		{
			direction = 0;
			coordSX = new Pair (0, rndm.nextInt((int) Toolkit.getDefaultToolkit().getScreenSize().getHeight()));
		}
		
		beforeTime = System.currentTimeMillis();
		delay = 700;
		animation = true;
		velocity = 4;
			
	}
	
	@Override
	public void move(long _currentTime) 
	{
		super.move(_currentTime, velocity);
	}
	
	@Override
	public boolean Collide(ICollidable tmp) 
	{
		Player ship = (Player) tmp;
		int AX,AY,BX,BY;
		BX = ship.coordSX.x + ship.data.x;
		BY = ship.coordSX.y + ship.data.y;
		AX = coordSX.x;
		AY = coordSX.y;
		return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
	}
	
	public int getImage()
	{
		if (direction == 1)
			return 0;
		return 1;
	}
	
	public int getHealthPoint()
	{
		return 10;
	}
}
