package Pirates_Assault;

import java.io.Serializable;

public class Pair implements Serializable{
	
	private static final long serialVersionUID = -5783680055972167097L;
	
	int x, y;
	
	public Pair (int _x, int _y){
		x = _x;
		y = _y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	@Override
	public String toString() {
		String tmp = new String ("X: " + x + " Y: " + y);
		return tmp;
	}
	
	@Override
	public boolean equals(Object arg0) {
		if (arg0 instanceof Pair){
			Pair tmp = (Pair) arg0;
			return (this.x == tmp.x && this.y == tmp.y);
		}
		else
			return false;
		
	}
	
	public void copy (Pair tmp){
		this.x = tmp.x;
		this.y = tmp.y;
	}
	
}
