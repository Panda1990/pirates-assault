package Pirates_Assault;

import java.io.Serializable;

public interface ICollidable extends Serializable {
	
	public boolean Collide (ICollidable tmp);
}
