package Pirates_Assault;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import application.Info;
import application.Info.Information;

public class Game implements IModGame {

	private static final long serialVersionUID = 4008747789761709975L;
	
	public List<Enemy> enemies;
	public List<ScenicContent> scenic;
	public List<Explosion> explosions;
	public List<Destroyed> destroyed;
	public List<Splash> splash;
	public Island [] islands;
	public VisibleScreen screen;
	static final public Pair dimensions = Info.Information.dimensions;
	Player player;
	BooleanRandom rndm;
	RedCircle redCircle;
	public AnimationFire animationFire;
	int spawn;
	
	boolean [] win;
	
	Detect detect;
	int intelligent;
	Class c = null;
	
	public Game (Player _player, int _intelligent) {
		enemies = new LinkedList<>();
		scenic = new LinkedList<>();
		explosions = new LinkedList<Explosion>();
		destroyed = new LinkedList<Destroyed>();
		splash = new LinkedList<Splash>();
		
		islands = new Island [5];
		islands [0] = new Island (dimensions.x + dimensions.x /4, 0);
		islands [1] = new Island (dimensions.x * 3 + dimensions.x /4, dimensions.y + dimensions.y / 4);
		islands [2] = new Island (0, dimensions.y * 4);
		islands [3] = new Island (dimensions.x * 3, dimensions.y * 3 + dimensions.y / 2);
		islands [4] = new Island (dimensions.x * 5 / 2, dimensions.y * 5 / 2);
		//islands [4] = new Island (dimensions.x, dimensions.y); NON SERVE, SOLO PER PROVA
		
		win = new boolean [islands.length];
		for (int i = 0; i < win.length; i++) 
			win[i] = false;
		
		redCircle = null;
		player = _player;
		screen = new VisibleScreen();
		rndm = new BooleanRandom();
		spawn = 10;
		
		this.detect = Info.Information.detect;
		intelligent = _intelligent;
		if (intelligent != -1){
			try{
				c = Class.forName(detect.fileInJar.get(intelligent).getName());
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void move_screen (int direction){
		boolean move = false;
		switch (direction){
		case 0:
			if (screen.goRight())
				move = true;
			break;
		case 1:
			if (screen.goLeft())
				move = true;
			break;
		case 2:
			if (screen.goUp())
				move = true;
			break;
		case 3:
			if (screen.goDown()){
				move = true;
			}break;
		default:
			break;
		
		}
		
		if (move){
			for (ScenicContent tmp: scenic){
				tmp.move_screen(direction, screen.velocity);
			}
			
			for (Enemy tmp : enemies){
				tmp.move_screen(direction, screen.velocity);
			}
			
			for (int i = 0; i < islands.length; i++)
				islands [i].move_screen(direction, screen.velocity);
			
			for (Destroyed tmp : destroyed) {
				tmp.move_screen(direction, screen.velocity);
			}
			
			for (Splash tmp : splash) {
				tmp.move_screen(direction, screen.velocity);
			}
			
			for (Explosion tmp : explosions){
				tmp.move_screen(direction, screen.velocity);
			}
		}
	}
	
	public void null_circle (){
		redCircle = null;
	}
	
	public void red_circle (){
		redCircle = new RedCircle(player.stats.cannon.cannonBall.range);
	}
	
	public RedCircle getCircle (){
		return redCircle;
	}
	
	public boolean getRedCircle (){
		return redCircle.getRedCircle();
	}
	
	public void setRedCircle (boolean _red){
		redCircle.setRedCircle(_red);
	}
	
	public void refresh (){
		
		long currentTime = System.currentTimeMillis();
		
		for (Iterator <ScenicContent> i = scenic.iterator(); i.hasNext(); )
		{
			ScenicContent scenic_tmp = (ScenicContent) i.next();
				
				if (scenic_tmp.Collide(player) || scenic_tmp.getCoordSX().x + 30 <= 0 || scenic_tmp.getCoordSX().x >= dimensions.x + 10 ||
						scenic_tmp.getCoordSX().y + 30 <= 0 || scenic_tmp.getCoordSX().y - 10 >= dimensions.y)
				
				{
					if (scenic_tmp instanceof Trunk)
					{
						player.setAmmo(((Trunk) scenic_tmp).getAmmo());
						i.remove();
					}
					
					if (scenic_tmp instanceof Barrel)
					{
						player.setHealthPoint(((Barrel) scenic_tmp).getHealthPoint());
						i.remove();
					}
					
					if (scenic_tmp instanceof Bottle)
					{
						player.checkBonus(((Bottle) scenic_tmp).getBonus());
						i.remove();
					}
				}
				else
				{
					scenic_tmp.move(currentTime);
					if (scenic_tmp instanceof Fish && (scenic_tmp.getCoordSX().x + 30 <= 0 || scenic_tmp.getCoordSX().x >= dimensions.x + 10 ||
							scenic_tmp.getCoordSX().y + 30 <= 0 || scenic_tmp.getCoordSX().y - 10 >= dimensions.y))
						i.remove();
	
				}
		}
		
		for (Iterator<Enemy> it = enemies.iterator(); it.hasNext();) {
			Enemy enemy = (Enemy) it.next();
			
			if (enemy.Collide(player)){
				enemy.getStats().setHealth_point(enemy.getStats().getHealth_point() - player.stats.physic_attack);
				player.stats.setHealth_point(player.stats.getHealth_point() - enemy.stats.physic_attack);
			}
			
			boolean move = true;
			boolean remove = false;
			if (enemy.getStats().getHealth_point() <= 0){
				destroyed.add(new Destroyed (enemy.getCoordSX().x, enemy.getCoordSX().y));
				it.remove();
				player.setDobloni(enemy.getDobloni());
				remove = true;
			}
			if (enemy.getCoordSX().x + enemy.getData().x + 10 <= 0 || enemy.getCoordSX().x - 10 >= dimensions.x ||
					enemy.getCoordSX().y + enemy.getData().y + 10 <= 0 || enemy.getCoordSX().y - 10 >= dimensions.y){
					it.remove();
					remove = true;
			}
			if (!remove){
				for (Enemy tmp: enemies)
					if (enemy != tmp && enemy.Collide(tmp)){
						enemy.switch_direction();
						move = false;
						break;
					}
				for (int i = 0; i < islands.length; i++){
					if (enemy.Collide(islands [i]))
					{
						enemy.switch_direction();
					}
				}
						
				if (move)
				{
					enemy.move(currentTime, player);
				}
				if(enemy.Fire(this.player))
				{
					Information.audio.playSparo();
					explosions.add(new Explosion (player.getPosX() + (player.getData().x / 2), player.getPosY() + (player.getData().y / 2)));
				}
						
			}
					
		}
		
		if(player.power)
		{
			player.durationPowerUp(currentTime);
		}
		
		player.check_position(this);
		player.move(currentTime);
		
		screen.velocity = player.stats.velocity;
			if (player.getPosX() >= dimensions.x - 250 && screen.goRight()){
				player.getCoordSX().setX(dimensions.x - 251);
				screen.move(0);
				move_screen(0);
			}
			else if (player.getPosX() <= 250 && screen.goLeft()){
				player.getCoordSX().setX(251);
				System.out.println("Left");
				screen.move(1);
				move_screen(1);
			}
			else if (player.getPosY() <= 250 && screen.goUp()){
				player.getCoordSX().setY(251);
				screen.move(2);
				move_screen(2);
			}
			else if (player.getPosY() >= dimensions.y - 250 && screen.goDown() && player.barchetta){
				player.getCoordSX().setY(dimensions.y - 251);
				screen.move(3);
				move_screen(3);
			}
			else if (player.getPosY() + 100 >= dimensions.y - 250 && screen.goDown() && !player.barchetta){
				player.getCoordSX().setY(dimensions.y - 251 - 100);
				screen.move(3);
				move_screen(3);
			}
		
	}

	public void check (){
		if (scenic.size() < 10){
			if (rndm.booleanRandom(400)) // generazione bottiglie random
			{
				boolean tmp = rndm.booleanRandom(2); // generazione bottle
				scenic.add(new Bottle(tmp));
			}
			
			if (rndm.booleanRandom(400)) // generazione barrel
			{
				boolean tmp = rndm.booleanRandom(2);
				scenic.add (new Barrel(tmp));
			}
			
			if (rndm.booleanRandom(100)) // generazione fish
			{
				boolean tmp = rndm.booleanRandom(2);
				scenic.add(new Fish(tmp));
			}
			
			if (rndm.booleanRandom(400)) // generazione trunk
			{
				boolean tmp = rndm.booleanRandom(2); 
				scenic.add(new Trunk(tmp));
			}
		}
		
		if (enemies.size() < spawn){
			Object ob = null;
			if (intelligent != -1){
				try{
					ob = c.newInstance();
				}catch (Exception e) {}
			}
			else
				ob = null;
			
			if (rndm.booleanRandom(100)) // generazione schooner random
			{
				boolean tmp1 = rndm.booleanRandom(2);
				Schooner tmp = new Schooner (tmp1);
				tmp.setIAO (ob);
				if (create (tmp))
					enemies.add(tmp);
			}
			
			else if (rndm.booleanRandom(200)) // generazione stormtrooper random
			{
				boolean tmp1 = rndm.booleanRandom(2);
				Stormtrooper tmp = new Stormtrooper(tmp1);
				tmp.setIAO (ob);
				if (create (tmp))
					enemies.add(tmp);
			}
			
			else if (rndm.booleanRandom(1000)) // generazione inquisitor random
			{
				boolean tmp1 = rndm.booleanRandom(2);
				Inquisitor tmp = new Inquisitor(tmp1);
				tmp.setIAO (ob);
				if (create (tmp))
					enemies.add(tmp);
			}
		}
		}
	
	private boolean create (Object_Game icol){
		boolean born = true;
		for (Enemy ship: enemies){
				int AX,AY,BX,BY;
				BX = ship.coordSX.x + ship.data.x;
				BY = ship.coordSX.y + ship.data.y;
				AX = icol.coordSX.x + icol.data.x;
				AY = icol.coordSX.y + icol.data.y;
				born = !(AX < ship.coordSX.x) || (BX < icol.coordSX.x) || (AY < ship.coordSX.y) || (BY < icol.coordSX.y);
			}
		
		for (int i = 0; i < islands.length; i++){
			Island island = islands [i];
			int AX,AY,BX,BY;
			BX = island.coordSX.x + island.data.x;
			BY = island.coordSX.y + island.data.y;
			AX = island.coordSX.x + island.data.x;
			AY = island.coordSX.y + island.data.y;
			born = !(AX < island.coordSX.x) || (BX < island.coordSX.x) || (AY < island.coordSX.y) || (BY < island.coordSX.y);
		}
		return born;
	}
	
	public boolean checkWin (){
		for (int i = 0; i < win.length; i++) 
			if (!win [i])
				return false;
		return true;
	}
	
	public void saveGame (){
		Info.Information.saveGame(this);
	}
}

