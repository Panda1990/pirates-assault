package Pirates_Assault;

public class AmmoPerTwo extends PowerUP{

	private static final long serialVersionUID = 1127985348892489688L;

	public AmmoPerTwo ()
	{
		delay = 900;
	}
	
	public int setAmmo(int index)
	{
		return index*2;
	}

	@Override
	public void move(long _delay) {
	}

	@Override
	public int getPosY() 
	{
		if (piccola)
		{
			return super.getPosY()-50;
		}
		else
		{
			return super.getPosY()-50;
		}
	}

	@Override
	public int getPosX() 
	{
		if (piccola)
		{
			if (player_direction == 0)
				return super.getPosX()+10;
			if (player_direction == 1)
				return super.getPosX()+20;
			else
				return super.getPosX();
		}
		else
		{
			if (player_direction == 0)
			{
				return super.getPosX()+30;
			}
			if (player_direction == 1)
			{
				return super.getPosX()+60;
			}
			else
			{
				return super.getPosX();
			}
		}
	}
	
	

}
