package Pirates_Assault;

import java.io.Serializable;

public class Destroyed implements Serializable {
	private static final long serialVersionUID = 8796273337992251458L;
	
	Pair coord;
	long currentTime;
	long delay;
	int index;
	
	public Destroyed (int x, int y){
		coord = new Pair (x, y);
		index = 0;
		currentTime = System.currentTimeMillis();
		delay = 600;
	}
	
	public Pair getCoord (){
		return coord;
	}
	
	public void updateIndex (){
		if (currentTime + delay < System.currentTimeMillis()){
			index++;
			currentTime = System.currentTimeMillis();
		}
	}
	
	public int getIndex (){
		return index;
	}
	
	public void move_screen (int _direction, int _velocity){
		switch (_direction){
		case 0: // go right;
			coord.x = coord.x - _velocity;
			break;
		case 1: // go left;
			coord.x = coord.x + _velocity;
			break;
		case 2: // go up;
			coord.y = coord.y + _velocity;
			break;
		case 3: // go down;
			coord.y = coord.y - _velocity;
			break;
		default:
			break;
		}
	
	}
	
}
