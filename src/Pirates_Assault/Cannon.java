package Pirates_Assault;

import java.io.Serializable;

public class Cannon implements Serializable{
	
	private static final long serialVersionUID = 5444382841029671813L;
	
	long beforeFire;
	long recharge;
	CannonBall cannonBall;
	
	public Cannon (long _beforeFire, long _recharge, CannonBall _cannonBall){
		beforeFire = _beforeFire;
		recharge = _recharge;
		cannonBall = _cannonBall;
	}

	public void setBeforeFire(long _beforeFire) {
		this.beforeFire = _beforeFire;
	}

	public void setRecharge(long recharge) {
		this.recharge = recharge;
	}

	public void setCannonBall(CannonBall cannonBall) {
		this.cannonBall = cannonBall;
	}

	public CannonBall getCannonBall() {
		return cannonBall;
	}
	
	
}
