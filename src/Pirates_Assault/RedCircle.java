package Pirates_Assault;

import java.io.Serializable;

public class RedCircle implements Serializable{

	private static final long serialVersionUID = 6632066560724767077L;
	
	boolean redCircle;
	long beforeCircle;
	long delay;
	int range;
	
	public RedCircle (int _range){
		redCircle = false;
		beforeCircle = System.currentTimeMillis();
		delay = 800;
		range = _range;
	}
	
	public boolean getRedCircle (){
		return redCircle;
	}
	
	public void setRedCircle (boolean _red){
		redCircle = _red;
	}

	public long getBeforeCircle() {
		return beforeCircle;
	}

	public void setBeforeCircle(long beforeCircle) {
		this.beforeCircle = beforeCircle;
	}

	public long getDelay() {
		return delay;
	}

	public void setDelay(long delay) {
		this.delay = delay;
	}

	public int getRange() {
		return range;
	}

	public void setRange(int range) {
		this.range = range;
	}
	
	
	

}
