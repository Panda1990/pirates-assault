package Pirates_Assault;

public class NewSecondIntelligence extends Intelligence{

	private static final long serialVersionUID = -7612538055290352685L;

	public void move (long _currentTime, Player hero) 
	{
		if (enemy.beforeTime + enemy.delay <= _currentTime){
			enemy.animation = !enemy.animation;
			
			switch (enemy.direction){
			case 0: // go right;
				enemy.coordSX.x = enemy.coordSX.x + enemy.stats.getVelocity();
				break;
			case 1: // go left;
				enemy.coordSX.x = enemy.coordSX.x - enemy.stats.getVelocity();
				break;
			case 2: // go up;
				enemy.coordSX.y = enemy.coordSX.y + enemy.stats.getVelocity();
				break;
			case 3: // go down;
				enemy.coordSX.y = enemy.coordSX.y - enemy.stats.getVelocity();
				break;
			default:
				break;
			}
			enemy.beforeTime = _currentTime;
		}
		return;
	}
	
	@Override
	public void switch_direction() {
			if (enemy.direction == 0){
				enemy.coordSX.x -= this.enemy.stats.getVelocity();
				enemy.direction = 1;
			}
			else if (enemy.direction == 1){
				enemy.coordSX.x += this.enemy.stats.getVelocity();
				enemy.direction = 0;
			}
			else if (enemy.direction == 3){
					enemy.coordSX.y -= this.enemy.stats.getVelocity();
					enemy.direction = 4;
			}
				else{
					enemy.coordSX.y += this.enemy.stats.getVelocity();
					enemy.direction = 3;
				}
				enemy.sprite = enemy.direction;
	}
}
