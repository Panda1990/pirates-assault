package Pirates_Assault;

import java.util.Random;

public class Inquisitor extends Enemy {

	// 0 right, 1 left, 2 up, 3 down
	
	private static final long serialVersionUID = 1572475212851314081L;

	public Inquisitor (boolean tmp){
		dobloni = 5;
		rndm = new Random ();
		stats = new Stats (new Cannon(10,  10,  new CannonBall(10, 500,  this)), 150, 200, 100, 15, 15, 1);
		direction = rndm.nextInt(4);
		
		switch (direction)
		{
		case 0:
			sprite = 0; 
			coordSX = new Pair(-180, rndm.nextInt(dimensions.y)); 
			data = new Pair (180, 174); 
			break;
		
		case 1:
			sprite = 1; 
			coordSX = new Pair((dimensions.x), rndm.nextInt(dimensions.y)); 
			data = new Pair (180, 147); 
			break;
		
		case 2: 
			sprite = 2; 
			coordSX = new Pair(rndm.nextInt(dimensions.x), -190); 
			data = new Pair (98, 190); 
			break;
		
		case 3:
			sprite = 3; 
			coordSX = new Pair(rndm.nextInt(dimensions.x), (dimensions.y)); 
			data = new Pair (95, 186); 
			break;
		}
		beforeTime = System.currentTimeMillis();
		delay = 100;
		animation = true;
	}
	
	public int getImage() 
	{
		if (sprite == 0)
		{
			data.x = 180;
			data.y = 147;
			if (animation)
				return 0;//right_1;
			else
				return 1;//right_2;
		}
		
		else if (sprite == 1)
		{
			data.x = 180;
			data.y = 147;
			if (animation)
				return 2;//left_1;
			else
				return 3;//left_2;
		}
		
		else if (sprite == 2)
		{
			data.x = 82;
			data.y = 186;
			if (animation)
				return 4;//up_1;
			else
				return 5;//up_2;
		}
		
		else
		{
			data.x = 76;
			data.y = 190;
			if (animation)
				return 6;//down_1;
			else
				return 7;//down_2;
		}
	}

	
	
}
