package Pirates_Assault;

import java.io.Serializable;

public interface IMovable extends Serializable {
	public void move (long _delay);
}
