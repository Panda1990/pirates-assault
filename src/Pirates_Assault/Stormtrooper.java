package Pirates_Assault;

import java.awt.Toolkit;
import java.util.Random;

public class Stormtrooper extends Enemy {

	private static final long serialVersionUID = 2672826465320210752L;

	// 0 right, 1 left, 2 up, 3 down
	
	public Stormtrooper (boolean tmp){
		dobloni = 3;
		rndm = new Random ();
		stats = new Stats (new Cannon(10,  10,  new CannonBall(15, 400, this)), 200, 80, 70, 10, 10, 3);
		direction = rndm.nextInt(4);

		switch (direction){
		case 0:
			sprite = 0; 
			coordSX = new Pair(-100, rndm.nextInt(dimensions.y)); 
			data = new Pair (60, 83); 
			break;
		
		case 1:
			sprite = 1; 
			coordSX = new Pair(dimensions.x, rndm.nextInt((int) Toolkit.getDefaultToolkit().getScreenSize().getHeight())); 
			data = new Pair (60, 83); 
			break;
		
		case 2: 
			sprite = 2; 
			coordSX = new Pair(rndm.nextInt(dimensions.x), -100); 
			data = new Pair (83, 60); 
			break;
		
		case 3:
			sprite = 3; 
			coordSX = new Pair(rndm.nextInt(dimensions.x), dimensions.y); 
			data = new Pair (83, 60); 
			break;
		}
		beforeTime = System.currentTimeMillis();
		delay = 92;
		animation = true;
	}
	
	public int getImage() {
		
		if (sprite == 0)
		{
			data.x = 128;
			data.y = 74;
			if (animation)
				return 0;//right_1;
			else
				return 1;//right_2;
		}
		
		else if (sprite == 1)
		{
			data.x = 128;
			data.y = 74;
			if (animation)
				return 2;//left_1;
			else
				return 3;//left_2;
		}
		
		else if (sprite == 2)
		{
			data.x = 64;
			data.y = 102;
			if (animation)
				return 4;//up_1;
			else
				return 5;//up_2;
		}
		
		else
		{
			data.x = 64;
			data.y = 102;
			if (animation)
				return 6;//down_1;
			else
				return 7;//down_2;
		}
	}
}
