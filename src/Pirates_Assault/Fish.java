package Pirates_Assault;

import java.util.Random;

public class Fish extends ScenicContent {
	
	private static final long serialVersionUID = -4718361190309909381L;
	private int left_sprite; // variabile che mi divide la list del pesce in destra e sinistra
	
	public Fish(boolean tmp)
	{
		rndm = new Random ();
		left_direction = tmp;
		left_sprite = 11;
		velocity = 10;
		data = new Pair (24, 24);
		
		if (tmp)
		{
			direction = 1;
			coordSX = new Pair (dimensions.x, rndm.nextInt(dimensions.y));
			sprite = rndm.nextInt(11);
			sprite += left_sprite;
		}
		else
		{
			direction = 0;
			coordSX = new Pair (0, rndm.nextInt(dimensions.y));
			sprite = rndm.nextInt(11);
		}
		
		beforeTime = System.currentTimeMillis();
		delay = 700;  
	}

	public int getImage ()
	{
		return sprite;
	}

	@Override
	public void move(long _currentTime) 
	{
		super.move(_currentTime, velocity);
	}
	
	public boolean Collide(ICollidable tmp) 
	{
		return false;
	}
	
}
