package Pirates_Assault;


public class Island extends ScenicContent {

	private static final long serialVersionUID = -8324834828046982852L;



	public Island (int _x, int _y){
		coordSX = new Pair (_x, _y);
		data = new Pair (1215, 1215);
	}

	@Override
	public void move(long _delay) {
		return;
	}

	
	
	@Override
	public boolean Collide(ICollidable tmp) {
		if (tmp instanceof Player){
			Player ship = (Player) tmp;
				int AX,AY,BX,BY;
				BX = ship.coordSX.x + ship.data.x;
				BY = ship.coordSX.y + ship.data.y;
				AX = coordSX.x + data.x;
				AY = coordSX.y + data.y;
				return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		
		else if (tmp instanceof Enemy){
			Enemy ship = (Enemy) tmp;
			int AX,AY,BX,BY;
			BX = ship.coordSX.x + ship.data.x;
			BY = ship.coordSX.y + ship.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		
		else if (tmp instanceof ScenicContent){
			ScenicContent content = (ScenicContent) tmp;
			int AX,AY,BX,BY;
			BX = content.coordSX.x + content.data.x;
			BY = content.coordSX.y + content.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < content.coordSX.x) || (BX < coordSX.x) || (AY < content.coordSX.y) || (BY < coordSX.y) );
		}
		
		return false;
	}

}
