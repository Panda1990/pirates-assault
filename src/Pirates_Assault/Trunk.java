package Pirates_Assault;

import java.util.Random;

public class Trunk extends ScenicContent{

	private static final long serialVersionUID = -3312898897977073185L;

	public Trunk (boolean tmp)
	{
		rndm = new Random ();
		left_direction = tmp;
		data = new Pair (0, 0);
		
		if (tmp)
		{
			direction = 1;
			coordSX = new Pair (dimensions.x, rndm.nextInt(dimensions.y));
		}
		else
		{
			direction = 0;
			coordSX = new Pair (0, rndm.nextInt(dimensions.y));
		}
		
		beforeTime = System.currentTimeMillis();
		delay = 700;
		animation = true;
		velocity = 4;
			
	}
	
	public int getImage ()
	{
		if (animation)
			return 0;
		else
			return 1;
	}
	
	@Override
	public void move(long _currentTime) 
	{
		super.move(_currentTime, velocity);
	}

	@Override
	public boolean Collide(ICollidable tmp) 
	{
		Player ship = (Player) tmp;
		int AX,AY,BX,BY;
		BX = ship.coordSX.x + ship.data.x;
		BY = ship.coordSX.y + ship.data.y;
		AX = coordSX.x;
		AY = coordSX.y;
		return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
	}
	
	public int getAmmo()
	{
		return 10;
	}
}
