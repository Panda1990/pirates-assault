package Pirates_Assault;

import java.util.List;

import application.Info;

public abstract class Object_Game implements ICollidable, IMovable, IFireAble {
	
	private static final long serialVersionUID = 5450860304486974620L;
	Pair coordSX; // coordinate del punto in alto a sx per il disegno e per la gabbia;
	Pair data; // due dati: la x descrive la lunghezza, y l'altezza;
	static final public Pair dimensions = Info.Information.dimensions;

	Stats stats; // Le sue caratteristiche
	
	
	@Override
	public int fire(int _x, int _y, List<Enemy> _enemies) {
		return -1;
	}
	
	@Override
	public boolean fire(Player _player) {
		return false;
	}

	public Stats getStats() {
		return stats;
	}

	public Pair getCoordSX() {
		return coordSX;
	}

	public Pair getData() {
		return data;
	}

	public void setCoordSX(Pair coordSX) {
		this.coordSX = coordSX;
	}

	public void setData(Pair data) {
		this.data = data;
	}

	public void setStats(Stats stats) {
		this.stats = stats;
	}
	
}
