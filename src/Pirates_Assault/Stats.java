package Pirates_Assault;

import java.io.Serializable;

public class Stats implements Serializable{
	
	private static final long serialVersionUID = 7876304959650380728L;
	
	Cannon cannon;
	int health_point;
	int cannon_attack;
	int physic_attack;
	int cannon_defense;
	int physic_defense;
	int velocity;
	
	public Stats (Cannon _cannon, int _health_point, int _cannon_attack, int _physic_attack, int _cannon_defense, int _pshysic_defense, int _velocity){
		cannon = _cannon;
		health_point = _health_point;
		cannon_attack = _cannon_attack;
		physic_attack = _physic_attack;
		cannon_defense = _cannon_defense;
		physic_defense = _pshysic_defense;
		velocity = _velocity;
	}
	
	public int getHealth_point() {
		return health_point;
	}

	public void setHealth_point(int health_point) {
		this.health_point = health_point;
	}

	public Cannon getCannon() {
		return cannon;
	}
	public void setCannon(Cannon cannon) {
		this.cannon = cannon;
	}
	public int getCannon_attack() {
		return cannon_attack;
	}
	public void setCannon_attack(int cannon_attack) {
		this.cannon_attack = cannon_attack;
	}
	public int getPhysic_attack() {
		return physic_attack;
	}
	public void setPhysic_attack(int physic_attack) {
		this.physic_attack = physic_attack;
	}
	public int getCannon_defense() {
		return cannon_defense;
	}
	public void setCannon_defense(int cannon_defense) {
		this.cannon_defense = cannon_defense;
	}
	public int getPhysic_defense() {
		return physic_defense;
	}
	public void setPhysic_defense(int physics_defense) {
		this.physic_defense = physics_defense;
	}
	public int getVelocity() {
		return velocity;
	}
	public void setVelocity(int velocity) {
		this.velocity = velocity;
	}
	
}
