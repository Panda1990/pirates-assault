package Pirates_Assault;

public class NewIntelligence extends Intelligence {
	
	private static final long serialVersionUID = -6884758536879418108L;

	NewIntelligence (){}

	public void move (long _currentTime, Player hero) 
	{
		if (enemy.beforeTime + enemy.delay <= _currentTime){
			enemy.animation = !enemy.animation;
			
			switch (enemy.direction){
			case 0: // go right;
				enemy.coordSX.x = enemy.coordSX.x + enemy.stats.getVelocity()*10;
				break;
			case 1: // go left;
				enemy.coordSX.x = enemy.coordSX.x - enemy.stats.getVelocity()*10;
				break;
			case 2: // go up;
				enemy.coordSX.y = enemy.coordSX.y + enemy.stats.getVelocity()*10;
				break;
			case 3: // go down;
				enemy.coordSX.y = enemy.coordSX.y - enemy.stats.getVelocity()*10;
				break;
			default:
				break;
			}
			enemy.beforeTime = _currentTime;
		}
		return;
	}
}
