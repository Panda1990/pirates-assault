package Pirates_Assault;

import java.util.Random;

import application.Info;

public abstract class ScenicContent implements IMovable, ICollidable {
	
	private static final long serialVersionUID = -3379562349937056908L;
	Pair coordSX;
	Pair data;
	static final public Pair dimensions = Info.Information.dimensions;
	int direction;
	int sprite;
	Random rndm;
	boolean left_direction;
	long beforeTime;
	long delay;
	boolean animation;
	int velocity;
	int bonus;
	
	public void move (long _currentTime, int velocity) 
	{
		if (beforeTime+delay <= _currentTime)
		{	
			animation = !animation;
			
			switch (direction){
			case 0: // go right;
				coordSX.x = coordSX.x + velocity;
				break;
			case 1: // go left;
				coordSX.x = coordSX.x - velocity;
				break;
			case 2: // go up;
				coordSX.y = coordSX.y - velocity ;
				break;
			case 3: // go down;
				coordSX.y = coordSX.y + velocity;
				break;
			default:
				break;
			}
			beforeTime = _currentTime;
		}
		return;
	}
	
	public void move_screen (int _direction, int _velocity){
		switch (_direction){
		case 0: // go right;
			coordSX.x = coordSX.x - _velocity;
			break;
		case 1: // go left;
			coordSX.x = coordSX.x + _velocity;
			break;
		case 2: // go up;
			coordSX.y = coordSX.y + _velocity;
			break;
		case 3: // go down;
			coordSX.y = coordSX.y - _velocity;
			break;
		default:
			break;
		}
	}
	
	public int getImage ()
	{
		if (!left_direction)
			return 0;
		else
			return 1;
	}

	public Pair getCoordSX() 
	{
		return coordSX;
	}

	public Pair getData() {
		return data;
	}

	
	
}
