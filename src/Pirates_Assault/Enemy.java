package Pirates_Assault;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Random;

public abstract class Enemy extends Object_Game {
	
	private static final long serialVersionUID = 1116148313733623127L;
	int direction;
	Random rndm;
	long beforeTime;
	long delay;
	boolean animation;
	int sprite;
	protected int dobloni;
	Intelligence IA = null;
	Object IAO = null;
	
	@Override
	public boolean Collide(ICollidable tmp) {
		if (tmp instanceof CannonBall){
			CannonBall ball = (CannonBall) tmp;
			if (ball.sender instanceof Player){
				int AX,AY,BX,BY;
				BX = ball.coordSX.x + ball.data.x;
				BY = ball.coordSX.y + ball.data.y;
				AX = coordSX.x + data.x;
				AY = coordSX.y + data.y;
				return  !( (AX < ball.coordSX.x) || (BX < coordSX.x) || (AY < ball.coordSX.y) || (BY < coordSX.y) );
			}
		}
		
		else if (tmp instanceof Player){ // FORSE SI PUO EVITARE
			Player ship = (Player) tmp;
			int AX,AY,BX,BY;
			BX = ship.coordSX.x + ship.data.x;
			BY = ship.coordSX.y + ship.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		
		else if (tmp instanceof Enemy){
			Enemy ship = (Enemy) tmp;
			int AX,AY,BX,BY;
			BX = ship.coordSX.x + ship.data.x;
			BY = ship.coordSX.y + ship.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < ship.coordSX.x) || (BX < coordSX.x) || (AY < ship.coordSX.y) || (BY < coordSX.y) );
		}
		
		else if (tmp instanceof Island){
			Island island = (Island) tmp;
			int AX,AY,BX,BY;
			BX = island.coordSX.x + island.data.x;
			BY = island.coordSX.y + island.data.y;
			AX = coordSX.x + data.x;
			AY = coordSX.y + data.y;
			return  !( (AX < island.coordSX.x) || (BX < coordSX.x) || (AY < island.coordSX.y) || (BY < coordSX.y) );
		}
		return false;
	}
	
	void setIAO (Object _IAO){
		if (_IAO == null)
			IA = new Intelligence(this);
		else{
			IAO = _IAO;
			try{
				Class [] args = new Class [1];
				args [0] = Enemy.class;
				Method m = IAO.getClass().getMethod("setEnemy", args);
				m.invoke(IAO, this);
			}catch (InvocationTargetException ie) {
				ie.getTargetException();
		}catch (Exception e) {
			e.printStackTrace();
		}
		}
	}
	
	public void move (long _currentTime, Player hero){
		if (IAO != null){
			try {
					Class [] args = new Class [2];
					args [0] = long.class;
					args [1] = Player.class;
					Method m = IAO.getClass().getMethod("move", args);
					if (m != null){
						m.setAccessible(true);
						m.invoke(IAO, _currentTime, hero);
					}
			}catch (InvocationTargetException ie){
				ie.getTargetException();
				IA.move(_currentTime, hero);
			}catch (Exception e) {
				e.printStackTrace();
				IA.move(_currentTime, hero);
			}
		}
		else
			IA.move(_currentTime, hero);
	
}
	
	public void move_screen (int _direction, int _velocity){
		switch (_direction){
		case 0: // go right;
			coordSX.x = coordSX.x - _velocity;
			break;
		case 1: // go left;
			coordSX.x = coordSX.x + _velocity;
			break;
		case 2: // go up;
			coordSX.y = coordSX.y + _velocity;
			break;
		case 3: // go down;
			coordSX.y = coordSX.y - _velocity;
			break;
		default:
			break;
		}
	
	}
	
	public void switch_direction (){
		if (IAO != null){
			try {
					Class [] args = null;
					Method m = IAO.getClass().getMethod("switch_direction", args);
					if (m != null){
						m.setAccessible(true);
						m.invoke(IAO);
					}
			}catch (InvocationTargetException ie){
				ie.getTargetException();
				IA.switch_direction ();
			}catch (Exception e) {
				e.printStackTrace();
				IA.switch_direction();
			}
		}
		else
			IA.switch_direction ();
	}
	
	public boolean Fire (Player hero){
		if (System.currentTimeMillis() >= stats.cannon.beforeFire + stats.cannon.recharge){
			stats.cannon.beforeFire = System.currentTimeMillis();
			return this.stats.cannon.cannonBall.fire(hero);
		}
		return false;
	}
	
	
	public void move (long _delay){}
	
	public int getDobloni()
	{
		return dobloni;
	}
}


