/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PirateOnline;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.sun.corba.se.impl.orbutil.ObjectWriter;

import GameOnline.GameOnline;
import Graphics.GameJPanel.Mouse_Adapter;

/**
 *
 * @author aruffolo
 */
public class PirateClientTCP implements Runnable
{   
    private Socket socket;
    private OutputStreamWriter outStreamWriter;
    private PrintWriter printWriter;
    private BufferedReader buffReader;
    private Thread thread;
    private int gameMode;
    private boolean playerAvaiable;
    private int portToConnect;
    private String addressToConnect;
    private String serverAddress;
    private int serverPort;
    private Object lock;
    private boolean answerReceived;
    private int portToWait;
    private ObjectOutputStream objectWriter;
    private ObjectInputStream objectPrinter;
    private List<Observer> multiplayer;
    
    private boolean gameEventsSender;
    
    public void setObserver(Observer obs) {
    	if (multiplayer == null)
    		multiplayer = new ArrayList<Observer>();
    	multiplayer.add(obs);
	}
    
    public PirateClientTCP() {}
    
    
    public ObjectInputStream getObjectPrinter() {
    	if (objectPrinter == null){
			try {
				objectPrinter = new ObjectInputStream(this.getSocket().getInputStream());
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
		return objectPrinter;
	}
    
    public ObjectOutputStream getObjectWriter() {
    	if (objectWriter == null){
			try {
				objectWriter = new ObjectOutputStream(this.getSocket().getOutputStream());
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
		return objectWriter;
	}
    
	public OutputStreamWriter getOutStreamWriter() throws UnknownHostException, IOException 
    {
        if(outStreamWriter == null)
        {
            outStreamWriter = new OutputStreamWriter(this.getSocket().getOutputStream());
        }
        return outStreamWriter;
    }

    public void setOutStreamWriter(OutputStreamWriter outStreamWriter) 
    {
        this.outStreamWriter = outStreamWriter;
    }

    public PrintWriter getPrintWriter() throws UnknownHostException, IOException {
        if(printWriter == null){
            printWriter = new PrintWriter(new BufferedWriter(this.getOutStreamWriter()), true);
        }
        return printWriter;
    }

    public void setPrintWriter(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public BufferedReader getBuffReader() throws UnknownHostException, IOException {
        if(buffReader == null){
            buffReader = new BufferedReader(new InputStreamReader(this.getSocket().getInputStream()));
        }
        return buffReader;
    }
    
    public Socket getSocket() throws UnknownHostException, IOException {
        if(socket == null){
            socket = new Socket(this.serverAddress, this.serverPort);
        }
        return socket;
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }
    
    public void setServerAddress(String serverAddress) {
        this.serverAddress = serverAddress;
    }
    
    public void setServerPort(int serverPort) {
        this.serverPort = serverPort;
    }

    public boolean isGameEventsSender()
    {
        return gameEventsSender;
    }

    public void setGameEventsSender(boolean gameEventsSender)
    {
        this.gameEventsSender = gameEventsSender;
    }        
    
    public void start() {
        thread = new Thread(this);
        thread.start();
    }

    @Override
    public void run() 
    {        
        if (this.gameEventsSender)
        {
            this.gameEventsSender();
        }
        else
        {
            this.gameLogicReceiver();
        }                            
    }
    
    private synchronized void gameLogicReceiver()
    {
        try {
            this.getPrintWriter().println("SS");
            
            String sss = this.getBuffReader().readLine();
            int id = Integer.parseInt(sss);
            if (id != -1)
            {
            	System.out.println("Client Riceve: " + id);
            	for (Observer observer : multiplayer) {
					observer.modify(id);
            	}
                //System.out.println("Vediao cosa sta facendo");
                this.getPrintWriter().println("SSSS");
                 //System.out.println("PRIMA WHILE");               
                while(true)
                {
                	GameOnline go = (GameOnline) this.getObjectPrinter().readObject();
                	//System.out.println("Arrivato");
                	//System.out.println("POSIZIONE: " + go.getPlayer().getPosX());
                	//System.out.println("E' arrivato il gioco" + (go instanceof GameOnline));
                	for (Observer observer : multiplayer) {
                		//System.out.println("OBSERVER DONE" + multiplayer.size());
						observer.modify(go);
                	}
                    /*sss = this.getBuffReader().readLine();
                    if (sss.equals("SSSSS"))
                    {
                        //System.out.println("Vediao cosa sta facendo 2");                             
                    }*/
                }                
            }
                    
        } catch (Exception e) 
        {
        	e.printStackTrace();
        	System.out.println("nel cliend : ");
            System.err.println(e);
        }
    }
    
    private void gameEventsSender()
    {
        /*try 
        {
            this.getPrintWriter().println("SS");            
            String sss = this.getBuffReader().readLine();
            
            if (sss.equals("SSS"))            
            {
                // Attesa di eventi;
            }
                    
        } catch (Exception e) 
        {
            System.err.println(e);
        }*/
    }
    
    public void eventReceived (Object event){
    	if (event == null)
    		return;
    	try {
			this.getObjectWriter().writeObject(event);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}


