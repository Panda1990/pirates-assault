package PirateOnline;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import com.sun.glass.events.KeyEvent;

public class Mouse_Adapter implements MouseListener {
	
	private PirateClientTCP sender;
	
	public Mouse_Adapter(PirateClientTCP _sender) {
		sender = _sender;
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		this.sender.eventReceived(changeToString(arg0));

	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	public static String changeToString (MouseEvent event){
		int key = event.getButton();
		String fire = null;
		if (key == MouseEvent.BUTTON1 || key == MouseEvent.BUTTON3 || key == MouseEvent.BUTTON2)
			fire = "Click";
		
		return fire;
	}

}
