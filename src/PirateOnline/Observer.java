package PirateOnline;

public interface Observer {
	public void modify (Object O);
}
