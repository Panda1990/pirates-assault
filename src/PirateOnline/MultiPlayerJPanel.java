package PirateOnline;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.TimerTask;

import javax.swing.JPanel;

import com.sun.media.jfxmedia.events.PlayerStateEvent.PlayerState;

import GameOnline.AmmoPerTwoOnline;
import GameOnline.AnimationFireOnline;
import GameOnline.BarrelOnline;
import GameOnline.BottleOnline;
import GameOnline.CannonBallOnline;
import GameOnline.CannonOnline;
import GameOnline.DestroyedOnline;
import GameOnline.ExplosionOnline;
import GameOnline.FishOnline;
import GameOnline.GameOnline;
import GameOnline.InfoOnline;
import GameOnline.InfoOnline.Information;
import application.Info;
import GameOnline.PairOnline;
import GameOnline.PirateTimerOnline;
import GameOnline.PlayerOnline;
import GameOnline.PowerUPOnline;
import GameOnline.ScenicContentOnline;
import GameOnline.StatsOnline;
import GameOnline.SuperShieldOnline;
import GameOnline.SuperSpeedOnline;
import GameOnline.TrunkOnline;

public class MultiPlayerJPanel extends JPanel implements Runnable, Observer {
	private static final long serialVersionUID = -7718822187758942506L;
	
	Thread thread;
	PirateClientTCP receiver;
	PirateClientTCP sender;
	GameOnline gameOnline = null;
	Loader loader;
	AnimationCoin ac;
	static final public PairOnline dimensions = InfoOnline.Information.dimensions;
	Information info;
	int tmp = 0;
	
	public MultiPlayerJPanel ()
	{
		super();
		loader = new Loader();
		loader.loadGame();
		gameOnline = new GameOnline(0);
		setFocusable(true);
		setDoubleBuffered(true);
		setVisible(true);
		thread = new Thread(this);
		this.createReceiver();
		this.createSender();
		addKeyListener(new Key_Adapter2(sender));
		addMouseListener(new Mouse_Adapter2 (sender));
		info = new Information();
		ac = new AnimationCoin(System.currentTimeMillis());
		//gameOnline = new GameOnline(0);
		//player = InfoOnline.Information.player.getplayer();
		/*if (player == null)
			InfoOnline.Information.player.setScreen(gameOnline.screen);*/
		thread.start();
		
	}
	
	public void createReceiver (){
		receiver = new PirateClientTCP();
		receiver.setObserver(this);
        receiver.setServerAddress("localhost"); //192.168.1.101 192.168.1.102
        receiver.setServerPort(5888); //6542 2002
        receiver.start();
	}
	
	public void createSender (){
		sender = new PirateClientTCP();
		sender.setGameEventsSender(true);
        sender.setServerAddress("localhost"); //192.168.1.101 192.168.1.102
        sender.setServerPort(5888); //6542 2002
        sender.start();
	}
	
	@Override
	protected void paintComponent(Graphics g) { 
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		if (gameOnline == null)
			return;
		g2d.drawImage(loader.getSea(gameOnline.screen.getMatrix_a().getX(), gameOnline.screen.getMatrix_a().getY()), (gameOnline.screen.getMatrix_a().getX() *  dimensions.getX()) - gameOnline.screen.getA().getX(), (gameOnline.screen.getMatrix_a().getY() *  dimensions.getY()) - gameOnline.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		g2d.drawImage(loader.getSea(gameOnline.screen.getMatrix_b().getX(), gameOnline.screen.getMatrix_b().getY()), (gameOnline.screen.getMatrix_b().getX() *  dimensions.getX()) - gameOnline.screen.getA().getX(), (gameOnline.screen.getMatrix_b().getY() *  dimensions.getY()) - gameOnline.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		g2d.drawImage(loader.getSea(gameOnline.screen.getMatrix_c().getX(), gameOnline.screen.getMatrix_c().getY()), (gameOnline.screen.getMatrix_c().getX() *  dimensions.getX()) - gameOnline.screen.getA().getX(), (gameOnline.screen.getMatrix_c().getY() *  dimensions.getY()) - gameOnline.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		g2d.drawImage(loader.getSea(gameOnline.screen.getMatrix_d().getX(), gameOnline.screen.getMatrix_d().getY()), (gameOnline.screen.getMatrix_d().getX() *  dimensions.getX()) - gameOnline.screen.getA().getX(), (gameOnline.screen.getMatrix_d().getY() *  dimensions.getY()) - gameOnline.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);

		ac.setCurrentTime(System.currentTimeMillis());
		
		for (Iterator<DestroyedOnline> it = gameOnline.destroyed.iterator(); it.hasNext();) {
			DestroyedOnline destroyed = (DestroyedOnline) it.next();
			int index = destroyed.getIndex();
			if (index < 5){
				g2d.drawImage(loader.getDestroyed(index), destroyed.getCoord().getX(), destroyed.getCoord().getY(), this);
				destroyed.updateIndex();
			}
			else
				it.remove();
		}
		
		for (ScenicContentOnline tmp: gameOnline.scenic)
		{
			if (tmp != null)
			{
				if (tmp instanceof BottleOnline)
				{
					g2d.drawImage((loader.getBottle(((BottleOnline) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
			
				if (tmp instanceof TrunkOnline)
				{	
					g2d.drawImage((loader.getTrunk(((TrunkOnline) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
				
				if (tmp instanceof FishOnline)
				{
					g2d.drawImage((loader.getFish(((FishOnline) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
				
				if (tmp instanceof BarrelOnline)
				{
					g2d.drawImage((loader.getBarrel(((BarrelOnline) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
			}
		}
		
		this.paintPlayerOnline(this.gameOnline.getOwnPlayer(), g2d);
		for (PlayerOnline tmp : gameOnline.players) {
			this.paintPlayerOnline(tmp, g2d);
		}
		
		for (Iterator <ExplosionOnline> it = gameOnline.explosions.iterator(); it.hasNext();) {
			ExplosionOnline explosion = (ExplosionOnline) it.next();
			int index = explosion.getIndex();
			if (index < 25){
				g2d.drawImage(loader.explosion.get(index), explosion.getCoord().getX() - 32, explosion.getCoord().getY() - 32, this);
				explosion.updateIndex();
			}
			else
				it.remove();
		}
		
		for (int i = 0; i < 5 ; i++)
			g2d.drawImage(loader.getIsland(), gameOnline.islands [i].getCoordSX().getX(), gameOnline.islands[i].getCoordSX().getY(), this);
	
			
			g2d.drawImage(loader.getTools(), 150, 18, this);
			String q = ""+gameOnline.getOwnPlayer().getStats().getHealth_point();
			g2d.drawString(q, 180, 35);
			
			g2d.drawImage((loader.getFlipCoin(ac.getIndex())), 10, 10, this);
			
			String s = ""+gameOnline.getOwnPlayer().getDobloni();
			g2d.drawString(s, 40, 35);
			
			g2d.drawImage(loader.getAnimationAmmo(), 70, 10, this);
			String w = ""+gameOnline.getOwnPlayer().getAmmo();
			g2d.drawString(w, 110, 35);
			
			String c = "" + tmp;
			tmp ++;
			g2d.drawString(c, 40, 60);
		
		Toolkit.getDefaultToolkit().sync();// sincronizzo
		g.dispose(); // dispone
	}
	
	
	public void paintPlayerOnline (PlayerOnline _player, Graphics2D g2d){
		
		g2d.drawImage(loader.getPlayer(_player.getImagePlayer()), _player.getPosX(), _player.getPosY(), this);
		if (_player.getPower())
		{
			PowerUPOnline p = _player.getPowerUp();
			if (p instanceof SuperShieldOnline)
			{
				if (_player.getBarchetta())
				{
					g2d.drawImage(loader.getSuperShield(), ((SuperShieldOnline) p).getPosX(), ((SuperShieldOnline) p).getPosY(), this);  
				}
				else
				{
					g2d.drawImage(loader.getBigSuperShield(), ((SuperShieldOnline) p).getPosX(), ((SuperShieldOnline) p).getPosY(), this);  
				}
			}
			
			if (p instanceof SuperSpeedOnline)
			{
				g2d.drawImage(loader.getSuperSpeed(((SuperSpeedOnline) p).getImage()), ((SuperSpeedOnline) p).getPosX(), ((SuperSpeedOnline) p).getPosY(), this);  
			}
			
			if (p instanceof AmmoPerTwoOnline)
			{
				g2d.drawImage(loader.getAmmo(), ((AmmoPerTwoOnline) p).getPosX(), ((AmmoPerTwoOnline) p).getPosY(), this);  
			}
		}
	}
	
	

	@Override
	public void run() {
		PirateTimerOnline pt = new PirateTimerOnline ();

		/*if (this.gameOnline != null){
			receiver.setObserver(this);
			System.out.println("SETTATO");
		}
		*/
		TimerTask tt = new TimerTask() {
		
		@Override
		public void run() 
		{
			//System.out.println("RUN");
			if (gameOnline != null){
				gameOnline.check();
				gameOnline.refresh();
				repaint();
			}
			
		}
	
	};
	pt.schedule(tt, 90, 90);
	
	}
	
	private synchronized void setGameOnline (GameOnline _game){
		this.gameOnline.players = _game.players;
		this.gameOnline.explosions = _game.explosions;
		this.gameOnline.destroyed = _game.destroyed;
		this.gameOnline.scenic = _game.scenic;
		
		for (PlayerOnline tmp : gameOnline.players) {
			if (tmp.getID() == this.gameOnline.getOwnPlayer().getID()){
				gameOnline.players.remove(tmp);
			}
		}
	}

	@Override
	public void modify(Object o) {
		if (o instanceof GameOnline){
			this.setGameOnline((GameOnline) o);
		}
		else if (o instanceof Integer){
			this.gameOnline.getOwnPlayer().setID((Integer) o);
			changePlayerPosition(gameOnline.getOwnPlayer());
		}
	}
	
	public void changePlayerPosition (PlayerOnline _player){
		_player.getCoordSX().setX(_player.getCoordSX().getX() + _player.getID() * 90);
		System.out.println("Change pos:" + _player.getCoordSX().getX());
	}
	
	public class Mouse_Adapter2 extends MouseAdapter{
		public Mouse_Adapter2(PirateClientTCP sender) {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			super.mouseClicked(e);
			int idColpito = -1;
			int ret = gameOnline.getOwnPlayer().fire(e.getX(), e.getY(), gameOnline.players, idColpito);
			
			switch (ret){
			case 0:
				System.out.println("CASO 0");
				gameOnline.explosions.add(new ExplosionOnline (e.getX(), e.getY()));
				if (idColpito != -1){
					String s = Mouse_Adapter.changeToString(e);
					s += "£" + idColpito; 
					System.out.println("FIEL: " + s);
					sender.eventReceived(s);
				}
				break;
			case 1:
				if (gameOnline.animationFire == null)
					gameOnline.animationFire = new AnimationFireOnline (true);
				break;
			case 3:
				if (gameOnline.animationFire == null)
					gameOnline.animationFire = new AnimationFireOnline(false);
				break;
			default:
				break;
			}
		}
	}
	
	private class Key_Adapter2 extends KeyAdapter{
		public Key_Adapter2(PirateClientTCP sender) {
			// TODO Auto-generated constructor stub
		}

		@Override
		public void keyPressed(KeyEvent arg0) {
			
			int key = arg0.getKeyCode();
			
			if (key == KeyEvent.VK_ESCAPE)
				Info.Information.popUpExit = true;
			
			super.keyPressed(arg0);
			gameOnline.getOwnPlayer().check_position(gameOnline);
			gameOnline.getOwnPlayer().keyPressed(key);
			
			String s = Key_Adapter.changeToString(arg0);
			s += "£" + gameOnline.getOwnPlayer().getID(); 
			sender.eventReceived(s);
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			super.keyReleased(arg0);
			gameOnline.getOwnPlayer().keyReleased(arg0);
		}
	
	}
}
