package PirateOnline;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Key_Adapter implements KeyListener {

	private PirateClientTCP sender;
	
	public Key_Adapter(PirateClientTCP _sender) {
		sender = _sender;
	}
	
	@Override
	public void keyPressed(KeyEvent arg0) {
		this.sender.eventReceived(changeToString (arg0));
	}

	@Override
	public void keyReleased(KeyEvent arg0) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}
	
	public static String changeToString (KeyEvent event){
		int key = event.getExtendedKeyCode();
		String move = null;
		switch (key){
		case KeyEvent.VK_UP:
			move = "Up";
			break;
		case KeyEvent.VK_RIGHT:
			move = "Right";
			break;
		case KeyEvent.VK_LEFT:
			move = "Left";
			break;
		case KeyEvent.VK_DOWN:
			move = "Down";
			break;
		case KeyEvent.VK_ESCAPE:
			move = "Esc";
			break;
		case KeyEvent.VK_SPACE:
			move = "Space";
			break;
		default:
			break;
		}
		return move;
	}

}
