package PirateOnline;

import java.awt.Label;

public class AnimationCoin {
	
	int index;
	int delay;
	long currentTime;
	long beforeTime;
	Label sum;
	
	public AnimationCoin (long _beforeTime)
	{
		index = 0;
		beforeTime = _beforeTime;
		delay = 100;
		sum = new Label();
	}
	
	public int getIndex()
	{
		if (beforeTime + delay <= currentTime)
		{
			beforeTime = currentTime;
			if (index < 3)
			{
				index ++;
				return index;
			}
			else
			{
				index = 0;
				return index;
			}
		}
		else
			return index;
	}
	
	public void setCurrentTime(long a)
	{
		currentTime = a;
	}
	
}
