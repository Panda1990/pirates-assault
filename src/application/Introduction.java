package application;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class Introduction {
	
	public Introduction() {}
	
	 public void showIntro()
	  {
	      try 
	      {
	      	FXMLLoader loader = new FXMLLoader();
	          loader.setLocation(MainMenu.class.getResource("Introduction.fxml"));
	          AnchorPane intro = (AnchorPane) loader.load();
	          Main.rootLayout.setCenter(intro);
	      } 
	      catch (IOException e) 
	      {
	          e.printStackTrace();
	      }
	  }

}
