package application;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.List;

import Graphics.PirateFrame;
import application.Info.Information;
import javafx.fxml.FXML;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;

public class SinglePlayerController {
	int intelligent = -1;
	boolean choosedGame = false;
	
	@FXML
	MenuButton mb;
	@FXML
	MenuItem mi0;
	@FXML
	MenuItem mi1;
	@FXML
	MenuItem mi2;
	@FXML
	MenuItem mi3;
	@FXML
	MenuItem mi4;
	@FXML
	MenuItem mi5;
	@FXML
	MenuItem mi6;
	@FXML
	MenuItem mi7;
	@FXML
	MenuItem mi8;
	@FXML
	MenuItem mi9;
	@FXML
	MenuItem mi10;
	
	@FXML
	MenuButton mb2;
	@FXML
	MenuItem mig0;
	@FXML
	MenuItem mig1;
	
	public SinglePlayerController() {}
	
	public void showSavedGame (){
		mig0.setText("New Game");
		int size = 0;
		if (Info.Information.isThereGames()){
			size = 1;
			System.out.println("QUALCOSA");
		}
		size = 1;
		List<MenuItem> items = new ArrayList<MenuItem>(size);
		items.add(mig1);
		
		for (int i = 0 ; i < size; i++){
			items.get(i).setVisible(true);
		}
	}
	
	@FXML
	public void showIntelligence(){
		mi0.setText("Intelligence");
		
		int size = Info.Information.detect.fileInJar.size();
		if (size > 10)
			size = 10;
		List<MenuItem> items = new ArrayList<MenuItem>(size);
		items.add(mi1);
		items.add(mi2);
		items.add(mi3);
		items.add(mi4);
		items.add(mi5);
		items.add(mi6);
		items.add(mi7);
		items.add(mi8);
		items.add(mi9);
		items.add(mi10);
		for (int i = 0 ; i < size; i++){
			items.get(i).setText(Info.Information.detect.fileInJar.get(i).getName());
			items.get(i).setVisible(true);
		}
	}
	
	public void setGame0 (){
		mb2.setText(mig0.getText());
		choosedGame = false;
	}
	

	public void setGame1 (){
		mb2.setText(mig1.getText());
		choosedGame = true;
	}

	
	public void setIntelligent0(){
		mb.setText(mi0.getText());
		intelligent = -1;
	}
	
	public void setIntelligent1(){
		mb.setText(mi1.getText());
		intelligent = 0;
	}
	
	public void setIntelligent2(){
		mb.setText(mi1.getText());
		intelligent = 1;
	}
	
	public void setIntelligent3(){
		mb.setText(mi1.getText());
		intelligent = 2;
	}
	
	public void setIntelligent4(){
		mb.setText(mi1.getText());
		intelligent = 3;
	}
	
	public void setIntelligent5(){
		mb.setText(mi1.getText());
		intelligent = 4;
	}
	
	public void setIntelligent6(){
		mb.setText(mi1.getText());
		intelligent = 5;
	}
	
	public void setIntelligent7(){
		mb.setText(mi1.getText());
		intelligent = 6;
	}
	
	public void setIntelligent8(){
		mb.setText(mi1.getText());
		intelligent = 7;
	}
	
	public void setIntelligent9(){
		mb.setText(mi1.getText());
		intelligent = 8;
	}
	
	public void setIntelligent10(){
		mb.setText(mi1.getText());
		intelligent = 9;
	}

	@FXML
	public void backToMenuPressed()
	{
		MainMenu m = new MainMenu(Info.Information.info);
		m.showMainMenu();
	}
	
	public void showDetails(){
	}
	
	@FXML
	public void playCampaing()
	{
		Information.audio.playGame();
		Information.setFrame(new PirateFrame(0, intelligent, choosedGame));
	}
	
	@FXML
	public void playSurvival (){
		Information.audio.playGame();
		new PirateFrame(1, intelligent, choosedGame);
	}
}
