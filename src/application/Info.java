package application;
import java.awt.Toolkit;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import Graphics.PirateFrame;
import Pirates_Assault.Cannon;
import Pirates_Assault.CannonBall;
import Pirates_Assault.Detect;
import Pirates_Assault.Game;
import Pirates_Assault.Pair;
import Pirates_Assault.Player;
import Pirates_Assault.Stats;

public class Info {
		public static class Information{		public static Player player;
		public static final Pair dimensions = new Pair ((int) Toolkit.getDefaultToolkit().getScreenSize().getWidth(), (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight());		public static Information info;		public static Detect detect;		
		public static boolean popUpExit;
		public static PlayerAudio audio;
		public static PirateFrame pf= null;
		public Information() {																															
			player = new Player(new Pair ((dimensions.getX()/2)-30, (dimensions.getY()/2)-50), new Pair(60, 100), new Stats (new Cannon (0, 1000, null), 150, 100, 70, 10, 8, 3));
			player.getStats().getCannon().setCannonBall(new CannonBall(20, 500, player));
			info = this;
			detect = new Detect ("///C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/plug_in");
			detect.search();
			popUpExit = false;
			audio = new PlayerAudio();
			Thread t = new Thread(audio);
			t.start();
		}
		public static void setFrame (PirateFrame _pf){			pf = _pf;
		}
		public static void setVisibilityFrame (boolean _vis){			pf.setVisible(_vis);
		}		
		public void setPlayer (Player _player){
			player = _player;
		}
		public static void saveGame (Game tmp){
			ObjectOutputStream output = null;
			if (tmp == null)
				System.out.println("NULL");
			System.out.println("SAVE");			try {				output = new ObjectOutputStream(new FileOutputStream("Input.txt"));
			} catch (IOException e) {
				e.printStackTrace();
			}			
			try {
				output.writeObject(tmp);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		public static Game loadGame (){
			ObjectInputStream input = null;
	        Game a = null;
	        
	        try {	        	input = new ObjectInputStream(new FileInputStream("Input.txt")); 
	        	} catch (FileNotFoundException e1) {
	        		e1.printStackTrace();
	        	} catch (IOException e1) {
	        		e1.printStackTrace();
	        	}
	        try {
	            a = (Game) input.readObject();
	            System.out.println("LOAD");
	        } catch (ClassNotFoundException e) {
	            e.printStackTrace();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	        if (a == null)
	        	System.out.println("NULL");
	        return a;
		}
		
		public static boolean isThereGames (){
			ObjectInputStream input = null;
	        try {
	            input = new ObjectInputStream(new FileInputStream("Input.txt"));
	        } catch (FileNotFoundException e1) {} 
	        	catch (IOException e1) {}
		        try {
		            input.readByte();
		            return true;
		        } catch (IOException e) {}
	        return false;
		}
	}
}
