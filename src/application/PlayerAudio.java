package application;

import java.io.File;
import java.io.IOException;
 
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineEvent;
import javax.sound.sampled.LineListener;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class PlayerAudio implements LineListener, Runnable{
	    boolean playCompleted;
	    boolean effetto = false;
	    String toPath;
	    
	    File audioFile;
	    AudioInputStream audioStream;
	    AudioFormat format;
	    DataLine.Info info;
	    Clip audioClip;
	    
	    public void playCredits (){
	    	playCompleted = true;
	    	toPath = "C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/Music/credits.wav";
	    	audioClip.close();
	    }
	    
	    public void playMenu (){
	    	playCompleted = true;
	    	toPath = "C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/Music/menu.wav";
	    	if (audioClip != null)
	    		audioClip.close();
	    }
	    
	    public void playShipYard (){
	    	playCompleted = true;
	    	toPath = "C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/Music/ShipYard.wav";
	    	audioClip.close();
	    }
	     
	    public void playGame (){
	    	playCompleted = true;
	    	toPath = "C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/Music/mare.wav";
	    	audioClip.close();
	    }
	    
	    public void playSparo(){
	    	PlayerAudio audio = new PlayerAudio ();
	    	Thread thrd = new Thread (audio);
	    	audio.effetto = true;
	    	audio.toPath = "C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/Music/sparo.wav";
	    	thrd.start();
	    }
	    
	    public void playSplash (){
	    	playSparo();
	    	PlayerAudio audio = new PlayerAudio ();
	    	Thread thrd = new Thread (audio);
	    	audio.effetto = true;
	    	audio.toPath = "C:/Users/Andrea/Documents/Workspace-Eclipse/Pirates_assault/pirates-assault/src/Music/splash.wav";
	    	thrd.start();
	    }
	    
	    void play() {
	    	if (!effetto)
	    		playCompleted = false;
	        audioFile = new File(toPath);
	 
	        try {
	            audioStream = AudioSystem.getAudioInputStream(audioFile);
	 
	            format = audioStream.getFormat();
	 
	            info = new DataLine.Info(Clip.class, format);
	 
	            audioClip = (Clip) AudioSystem.getLine(info);
	 
	            audioClip.addLineListener(this);
	 
	            audioClip.open(audioStream);
	             
	            audioClip.start();
	             
	            while (!playCompleted) {
	                try {
	                    Thread.sleep(1000);
	                } catch (InterruptedException ex) {
	                    ex.printStackTrace();
	                }
	            }
	             
	            audioClip.close();
	             
	        } catch (UnsupportedAudioFileException ex) {
	            System.out.println("The specified audio file is not supported.");
	            ex.printStackTrace();
	        } catch (LineUnavailableException ex) {
	            System.out.println("Audio line for playing back is unavailable.");
	            ex.printStackTrace();
	        } catch (IOException ex) {
	            System.out.println("Error playing the audio file.");
	            ex.printStackTrace();
	        }
	         
	    }
	    
	    @Override
	    public void update(LineEvent event) {
	        LineEvent.Type type = event.getType();
	        if (type == LineEvent.Type.STOP) {
	            playCompleted = true;
	        }
	 
	    }

		@Override
		public void run() {
			if (!effetto)
				while (true)
					this.play();
			else
				this.play();
			
		}
}
