package application;

import javafx.fxml.FXML;

public class HowToPlayController {
	
	@FXML
	public void backToMenuPressed()
	{
		MainMenu m = new MainMenu(Info.Information.info);
		m.showMainMenu();
	}

}
