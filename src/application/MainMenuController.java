package application;

import Graphics.PirateFrame;
import javafx.application.Application;
import javafx.fxml.FXML;


public class MainMenuController {
	
	
	@FXML
	public void singlePlayerPressed()
	{
		SinglePlayer s = new SinglePlayer();
		s.showSinglePlayer();
	}
	
	@FXML
	public void howToPressed()
	{
		HowToPlay h = new HowToPlay();
		h.showHowToPlay();
	}
	
	@FXML
	public void creditsPressed()
	{
		Credits s = new Credits();
		s.showCredits();
	}
	
	@FXML
	public void quitPressed()
	{
		System.exit(1);
	}
	@FXML
	public void multiPlayerPressed()
	{
		MultiPlayer m = new MultiPlayer();
		m.showMultiPlayer();

		/*new PirateFrame(2, 0);
		Main.setPrimaryStage(false);*/
	}
	
	@FXML
	public void shipYardPressed()
	{
		ShipYard sy = new ShipYard();
		sy.showShipYard();
	}
	
	@FXML
	public int getPosX()
	{
		return 100;
	}
}
