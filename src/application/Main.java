package application;
	
import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;


public class Main extends Application {
	
	 static Stage primaryStage;
	 static BorderPane rootLayout;
	 Introduction intro = null;
	 
	 public Main()
	 {
		 intro = new Introduction();
	 }
	    @Override
	    public void start(Stage primaryStage) 
	    {
	        Main.primaryStage = primaryStage;
	        initRootLayout();
	        intro.showIntro();
	    }

	    public void initRootLayout()
	    {
	        try 
	        {
	        	FXMLLoader loader = new FXMLLoader();
	            loader.setLocation(Main.class.getResource("RootLayout.fxml"));
	            rootLayout = (BorderPane) loader.load();
	            Scene scene = new Scene(rootLayout);
	            primaryStage.setScene(scene);
	            primaryStage.show();
	            primaryStage.setFullScreen(true);
	        } 
	        catch (IOException e) 
	        {
	            e.printStackTrace();
	        }
	    }
	   
	    public static Stage getPrimaryStage() 
	    {
	        return primaryStage;
	    }
	    
	    public static void setPrimaryStage(boolean b)
	    {
	    	if (b)
	    	{
	    		primaryStage.show();
	    	}
	    	else
	    	{
	    		primaryStage.close();
	    	}
	    }

	    public static void main(String[] args) {
	        launch(args);
	    }
}
