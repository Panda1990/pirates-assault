package application;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class MultiPlayer {

	
	public void showMultiPlayer (){
		try 
	      {
	    	  FXMLLoader loader = new FXMLLoader();
	          loader.setLocation(MultiPlayer.class.getResource("MultiPlayer.fxml"));
	          AnchorPane multi = (AnchorPane) loader.load();
	          Main.rootLayout.setCenter(multi);
	      } 
	      catch (IOException e) 
	      {
	          e.printStackTrace();
	      }
	}
}
