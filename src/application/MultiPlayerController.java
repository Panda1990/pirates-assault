package application;

import Graphics.PirateFrame;
import application.Info.Information;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

public class MultiPlayerController {
	
	@FXML
	TextField user;
	@FXML
	TextField port;
	@FXML
	PasswordField pass;
	@FXML
	Button start;
	@FXML
	Button back;
	
	String portToConnect;
	String username;
	String password;
	
	boolean [] canStart = new boolean [3];
	
	public void canAble (){
		for (int i = 0; i < 3; i++)
			if (!canStart [i])
				return;
		start.setDisable(false);
	}
	
	public void setPort (){
		portToConnect = port.getText();
		
		if (portToConnect.length() == 4)
			canStart [2] = true;
		if (portToConnect.length() != 4){
			start.setDisable(true);
			canStart [2] = false;
		}
		canAble ();
	}
	
	public void setUser (){
		username = user.getText();
		canStart [0] = true;
		
		canAble ();
	}
	
	public void setPassword (){
		password = pass.getText();
		canStart [1] = true;
		
		canAble ();
	}
	
	@FXML
	public void startGame(){
		Information.audio.playGame();
		new PirateFrame(2, 0, false);
		Main.setPrimaryStage(false);
	}
	
	
	@FXML
	public void backToMenuPressed()
	{
		MainMenu m = new MainMenu(Info.Information.info);
		m.showMainMenu();
	}
}
