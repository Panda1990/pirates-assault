package application;

import java.io.IOException;
import application.Info.Information;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class MainMenu {
	Information info = null;
	public MainMenu() {
		 info = new Information();
		Information.info.audio.playMenu(); 
	}
	
	public MainMenu (Information _info){
		info = _info;
	}
	
	 public void showMainMenu()
	  {
	      try 
	      {
	    	  FXMLLoader loader = new FXMLLoader();
	          loader.setLocation(MainMenu.class.getResource("MainMenu.fxml"));
	          AnchorPane mainMenu = (AnchorPane) loader.load();
	          Main.rootLayout.setCenter(mainMenu);
	      } 
	      catch (IOException e) 
	      {
	          e.printStackTrace();
	      }
	  }

}
