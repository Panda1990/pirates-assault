package application;

import Pirates_Assault.Pair;
import Pirates_Assault.Player;
import application.Info.Information;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ShipYardController {
	
	@FXML
	Label attack;
	@FXML
	Label defense;
	@FXML
	Label ram;
	@FXML
	Label velocity;
	@FXML
	Label small;
	@FXML
	Label big;	
	@FXML
	Label description;	
	@FXML
	Label coin;
	
	@FXML
	Button buy;
	
	
	Player player_tmp = new Player (Info.Information.player);
	@FXML
	
	public void refreshDescription (){
		description.setText("HP: " + player_tmp.getStats().getHealth_point() + "\n" + "Physic Attack: " + player_tmp.getStats().getPhysic_attack() +  "\n" +
				"Cannon Attack: " + player_tmp.getStats().getCannon_attack() + "\n" + "Physic Defense: " + player_tmp.getStats().getPhysic_defense() + "\n" +
				"Cannon Defense: " + player_tmp.getStats().getCannon_defense() + "\n" + "Velocity: " + player_tmp.getStats().getVelocity());
		
		coin.setText("Coin: " + player_tmp.getDobloni());
	}
	
	public void showStatsBigShip(){
		if (!Information.player.isBoth()){
			description.setText("HP: " + 300 + "\n" + "Physic Attack: " + 130 +  "\n" + "Cannon Attack: " + 180 + "\n" + "Physic Defense: " + 15 + "\n" + "Cannon Defense: " + 20 + "\n" + "Velocity: " + 2);
			coin.setText("Coin: " + player_tmp.getDobloni());
		}
		else
			refreshDescription();
	}
	
	
	@FXML
	public void attackPressed()
	{
		defense.setVisible(false);
		ram.setVisible(false);
		velocity.setVisible(false);
		small.setVisible(false);
		big.setVisible(false);
		
		refreshDescription();
		description.setVisible(true);
		attack.setVisible(true);
		buy.setText("Buy for 20$");
		if (player_tmp.getDobloni() < 20)
			buy.setDisable(true);
		else{
			buy.setDisable(false);
		}
	}
	
	@FXML
	public void defensePressed()
	{
		ram.setVisible(false);
		velocity.setVisible(false);
		small.setVisible(false);
		big.setVisible(false);
		attack.setVisible(false);
		
		refreshDescription();
		description.setVisible(true);
		defense.setVisible(true);
		buy.setDisable(false);
		buy.setText("Buy for 20$");
		if (player_tmp.getDobloni() < 20)
			buy.setDisable(true);
		else{
			buy.setDisable(false);
		}
	}
	
	@FXML
	public void ramPressed()
	{
		defense.setVisible(false);
		velocity.setVisible(false);
		small.setVisible(false);
		big.setVisible(false);
		attack.setVisible(false);

		refreshDescription();
		description.setVisible(true);
		ram.setVisible(true);
		buy.setDisable(false);
		buy.setText("Buy for 20$");
		if (player_tmp.getDobloni() < 20)
			buy.setDisable(true);
		else{
			buy.setDisable(false);
		}
			
	}
	
	@FXML
	public void velocityPressed()
	{
		ram.setVisible(false);
		defense.setVisible(false);
		small.setVisible(false);
		big.setVisible(false);
		attack.setVisible(false);

		refreshDescription();
		description.setVisible(true);
		velocity.setVisible(true);
		buy.setDisable(false);
		

		if (player_tmp.getStats().getVelocity() >= 7){
			buy.setText("Not Avaible");
			buy.setDisable(true);
		}
		if (player_tmp.getDobloni() < 20){
			buy.setText("Buy for 20$");
			buy.setDisable(true);
		}
		else if (player_tmp.getStats().getVelocity() < 7){
			buy.setDisable(false);
		}
	}
	
	@FXML
	public void smallShipPressed()
	{
		ram.setVisible(false);
		velocity.setVisible(false);
		defense.setVisible(false);
		big.setVisible(false);
		attack.setVisible(false);

		refreshDescription();
		description.setVisible(true);
		small.setVisible(true);
		buy.setDisable(false);
		buy.setText("Free");
	}
	
	@FXML
	public void bigShipPressed()
	{
		ram.setVisible(false);
		velocity.setVisible(false);
		small.setVisible(false);
		defense.setVisible(false);
		attack.setVisible(false);

		showStatsBigShip();
		description.setVisible(true);
		big.setVisible(true);
		buy.setDisable(false);
		if (player_tmp.isBoth())
			buy.setText("Free");
		else{
			if (player_tmp.getDobloni() < 200)
				buy.setDisable(true);
			else{
				buy.setDisable(false);
			}
			buy.setText("Buy for 200$");
		}
		player_tmp.setDobloni(2000);
	}
	
	@FXML
	public void buttonPressed(){
		if (attack.isVisible())
			buyAttack();
		else if (defense.isVisible())
			buyDefense();
		else if (ram.isVisible())
			buyRam();
		else if (velocity.isVisible())
			buyVelocity();
		else if (small.isVisible())
			selectSmallShip();
		else if (big.isVisible())
			buyBigShip();

		refreshDescription();
		description.setVisible(true);
	}
	
	@FXML
	public void buyAttack (){
		if (player_tmp.getDobloni() >= 20){
			player_tmp.getStats().setCannon_attack(player_tmp.getStats().getCannon_attack() + 1);
			player_tmp.setDobloni(player_tmp.getDobloni() - 20);
		}
		if (player_tmp.getDobloni() < 20)
			buy.setDisable(true);
	}
	
	@FXML
	public void buyDefense (){
		if (player_tmp.getDobloni() >= 20){
			player_tmp.getStats().setCannon_defense(player_tmp.getStats().getCannon_defense() + 1);
			player_tmp.getStats().setPhysic_defense(player_tmp.getStats().getPhysic_defense() + 1);
			player_tmp.setDobloni(player_tmp.getDobloni() - 20);
		}
		if (player_tmp.getDobloni() < 20)
			buy.setDisable(true);
	}
	
	@FXML
	public void buyRam (){
		if (player_tmp.getDobloni() >= 20){
			player_tmp.getStats().setPhysic_attack(player_tmp.getStats().getPhysic_attack() + 1);
			player_tmp.setDobloni(player_tmp.getDobloni() - 20);
		}
		if (player_tmp.getDobloni() < 20)
			buy.setDisable(true);
	}
	
	@FXML
	public void buyVelocity (){
		if (player_tmp.getDobloni() >= 20){
			player_tmp.getStats().setVelocity(player_tmp.getStats().getVelocity() + 1);
			player_tmp.setDobloni(player_tmp.getDobloni() - 20);
		}
			if (player_tmp.getDobloni() < 20)
				buy.setDisable(true);
	}
	
	@FXML
	public void selectSmallShip (){
		player_tmp.setBarchetta(true);
		player_tmp.setData(new Pair (49, 89));
	}
	
	@FXML
	public void buyBigShip (){
		if (player_tmp.getDobloni() >= 200){
			player_tmp.setBarchetta(false);
			player_tmp.setData(new Pair (99, 190));
			player_tmp.setBoth(true);
			buy.setText("Free");
			player_tmp.getStats().setHealth_point(300);
			player_tmp.getStats().setPhysic_attack(130);
			player_tmp.getStats().setCannon_attack(180);
			player_tmp.getStats().setPhysic_defense(15);
			player_tmp.getStats().setCannon_defense(20);
			player_tmp.getStats().setVelocity(2);
		}
		if (player_tmp.getDobloni() < 200)
			buy.setDisable(true);
	}
	
	@FXML
	public void backToMenuPressed()
	{
		Information.audio.playMenu();
		MainMenu m = new MainMenu(Info.Information.info);
		Info.Information.player = player_tmp;
		m.showMainMenu();
	}
	
}
