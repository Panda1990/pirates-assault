package application;

import java.io.IOException;

import application.Info.Information;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class Credits {
	
	public Credits() {}
	
	 public void showCredits()
	  {
		 Information.audio.playCredits();
	      try 
	      {
	          FXMLLoader loader = new FXMLLoader();
	          loader.setLocation(Credits.class.getResource("Credits.fxml"));
	          AnchorPane credits = (AnchorPane) loader.load();
	          Main.rootLayout.setCenter(credits);
	       } 
	      catch (IOException e) 
	      {
	          e.printStackTrace();
	      }
	    }


}
