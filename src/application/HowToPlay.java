package application;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class HowToPlay {
	
	public HowToPlay() {}
	
	 public void showHowToPlay()
	  {
	      try 
	      {
	          FXMLLoader loader = new FXMLLoader();
	          loader.setLocation(HowToPlay.class.getResource("HowToPlay.fxml"));
	          AnchorPane how = (AnchorPane) loader.load();
	          Main.rootLayout.setCenter(how);
	       } 
	      catch (IOException e) 
	      {
	          e.printStackTrace();
	      }
	    }

}
