package application;

import java.io.IOException;

import Pirates_Assault.Pair;
import Pirates_Assault.Player;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import application.Info.Information;

public class SinglePlayer {
	
	public SinglePlayer() {}
	
	 public void showSinglePlayer()
	 {
	     try 
	     {
	         FXMLLoader loader = new FXMLLoader();
	         loader.setLocation(SinglePlayer.class.getResource("SinglePlayer.fxml"));
	         AnchorPane single = (AnchorPane) loader.load();
	         Main.rootLayout.setCenter(single);
	      } 
	     catch (IOException e) 
	     {
	         e.printStackTrace();
	     }
	   }
}
