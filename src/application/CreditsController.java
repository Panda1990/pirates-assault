package application;

import application.Info.Information;
import javafx.fxml.FXML;

public class CreditsController {
	
	@FXML
	public void backToMenuPressed()
	{
		Information.audio.playMenu();
		MainMenu m = new MainMenu(Info.Information.info);
		m.showMainMenu();
	}

}
