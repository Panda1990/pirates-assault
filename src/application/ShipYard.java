package application;

import java.io.IOException;

import application.Info.Information;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class ShipYard {
	public ShipYard () {}
	
	 public void showShipYard()
	  {
		 Information.audio.playShipYard();
		 try 
	      {
	          FXMLLoader loader = new FXMLLoader();
	          loader.setLocation(ShipYard.class.getResource("ShipYard.fxml"));
	          AnchorPane ship = (AnchorPane) loader.load();
	          Main.rootLayout.setCenter(ship);
	       } 
	      catch (IOException e) 
	      {
	          e.printStackTrace();
	      }
	    }


}