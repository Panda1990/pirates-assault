package Graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JPanel;
import application.Info;
import application.Introduction;
import application.Main;
import application.Info.Information;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class ExitJPanel extends JPanel {

	private static final long serialVersionUID = 1L;
	Loader loader;
	JButton resume = null;
	JButton back = null;
	JButton quit = null;
	PopUpExit frame;
	
	public ExitJPanel(Loader _loader, PopUpExit _frame)
	{
		super();
		setFocusable(true);
		setDoubleBuffered(true);
		loader = _loader;
		resume = new JButton("Resume");
		back = new JButton("Back to Menu");
		quit = new JButton("Quit Game");
		this.setLayout(null);
		resume.setBounds(25, 150, 150, 50);
		back.setBounds(225 ,150 , 150, 50);
		quit.setBounds(125, 250, 150, 50);
		resume.setVisible(true);
		back.setVisible(true);
		quit.setVisible(true);
		add(resume);
		add(back);
		add(quit);
		resume.addActionListener(new Button_listener());
		quit.addActionListener(new Button_listener());
		back.addActionListener(new Button_listener());
		setVisible(true);
		frame = _frame;
		
		repaint();
	}
	
	@Override
	protected void paintComponent(Graphics g) 
	{		
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(loader.getPopUp(), 0, 0, this);
		resume.repaint();
		back.repaint();
		quit.repaint();
		Toolkit.getDefaultToolkit().sync();// sincronizzo
		g.dispose(); // dispone
		
		
	}

	class Button_listener implements ActionListener {
		  public void actionPerformed(ActionEvent e) {
		    		    JButton button = (JButton)e.getSource();
		    if (button.equals(resume))
		    {
		    	Info.Information.popUpExit = false;
		    	Info.Information.audio.playGame();
		    	frame.setVisibility(false);
		    }
		    
		    if (button.equals(quit))
		    {
		    	Info.Information.saveGame(frame.game);
		    	System.exit(1);
		    }
		    
		    if (button.equals(back))
		    {
		    	System.out.println("HERE");
		    	Info.Information.saveGame(frame.game);
		    	frame.setVisibility(false);
		    	Information.setVisibilityFrame(false);
		    }
		  }
	}
}
