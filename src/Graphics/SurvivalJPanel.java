package Graphics;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.TimerTask;
import javax.swing.JPanel;
import Pirates_Assault.AmmoPerTwo;
import Pirates_Assault.AnimationFire;
import Pirates_Assault.Barrel;
import Pirates_Assault.BooleanRandom;
import Pirates_Assault.Bottle;
import Pirates_Assault.Destroyed;
import Pirates_Assault.Enemy;
import Pirates_Assault.Explosion;
import Pirates_Assault.Fish;
import Pirates_Assault.Game;
import Pirates_Assault.Inquisitor;
import Pirates_Assault.Pair;
import Pirates_Assault.Player;
import Pirates_Assault.ScenicContent;
import Pirates_Assault.Schooner;
import Pirates_Assault.Splash;
import Pirates_Assault.Stormtrooper;
import Pirates_Assault.SuperShield;
import Pirates_Assault.SuperSpeed;
import Pirates_Assault.SurvivalMod;
import Pirates_Assault.Trunk;
import application.Info;
import application.Info.Information;
import Pirates_Assault.PowerUP;

public class SurvivalJPanel extends JPanel implements Runnable {

	private static final long serialVersionUID = 1L;

	Thread thread;
	Player player;
	Game game;
	BooleanRandom rndm;
	Loader loader;
	AnimationCoin ac;
	static final public Pair dimensions = Info.Information.dimensions;
	PopUpExit pue = null;
	SurvivalMod survival;
	
	public SurvivalJPanel(int intelligent) 
	{
		super();
		addKeyListener(new Key_Adapter());
		addMouseListener(new Mouse_Adapter ());
		setFocusable(true);
		setDoubleBuffered(true);
		setVisible(true);
		player = Info.Information.player.getPlayer();
		game = new Game (player, intelligent);
		if (player == null)
			Info.Information.player.setScreen(game.screen);
		survival = new SurvivalMod (player, intelligent);
		rndm = new BooleanRandom();
		loader = new Loader ();
		loader.loadGame();
		survival.explosions.add(new Explosion (-100, -100));
		thread = new Thread(this);
		rndm = new BooleanRandom();
		ac = new AnimationCoin(System.currentTimeMillis());
		thread.start();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.drawImage(loader.getSea(survival.screen.getMatrix_a().getX(), survival.screen.getMatrix_a().getY()), (survival.screen.getMatrix_a().getX() *  dimensions.getX()) - survival.screen.getA().getX(), (survival.screen.getMatrix_a().getY() *  dimensions.getY()) - survival.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		g2d.drawImage(loader.getSea(survival.screen.getMatrix_b().getX(), survival.screen.getMatrix_b().getY()), (survival.screen.getMatrix_b().getX() *  dimensions.getX()) - survival.screen.getA().getX(), (survival.screen.getMatrix_b().getY() *  dimensions.getY()) - survival.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		g2d.drawImage(loader.getSea(survival.screen.getMatrix_c().getX(), survival.screen.getMatrix_c().getY()), (survival.screen.getMatrix_c().getX() *  dimensions.getX()) - survival.screen.getA().getX(), (survival.screen.getMatrix_c().getY() *  dimensions.getY()) - survival.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		g2d.drawImage(loader.getSea(survival.screen.getMatrix_d().getX(), survival.screen.getMatrix_d().getY()), (survival.screen.getMatrix_d().getX() *  dimensions.getX()) - survival.screen.getA().getX(), (survival.screen.getMatrix_d().getY() *  dimensions.getY()) - survival.screen.getA().getY(), dimensions.getX(),  dimensions.getY(), this);
		ac.setCurrentTime(System.currentTimeMillis());

		for (Iterator <Destroyed> it = survival.destroyed.iterator(); it.hasNext();) {
			Destroyed destroyed = (Destroyed) it.next();
			int index = destroyed.getIndex();
			if (index < 5){
				g2d.drawImage(loader.getDestroyed(index), destroyed.getCoord().getX(), destroyed.getCoord().getY(), this);
				destroyed.updateIndex();
			}
			else
				it.remove();
		}
		
		for (Iterator <Splash> it = survival.splash.iterator(); it.hasNext();) {
			Splash splash = (Splash) it.next();
			int index = splash.getIndex();
			if (index < 2){
				g2d.drawImage(loader.getSplash(index), splash.getCoord().getX() - 35, splash.getCoord().getY() - 35, this);
				splash.updateIndex();
			}
			else
				it.remove();
		}
		
		for (ScenicContent tmp: survival.scenic)
		{
			if (tmp != null)
			{
				if (tmp instanceof Bottle)
				{
					g2d.drawImage((loader.getBottle(((Bottle) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
			
				if (tmp instanceof Trunk)
				{	
					g2d.drawImage((loader.getTrunk(((Trunk) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
				
				if (tmp instanceof Fish)
				{
					g2d.drawImage((loader.getFish(((Fish) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
				
				if (tmp instanceof Barrel)
				{
					g2d.drawImage((loader.getBarrel(((Barrel) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
			}
		}
		
		if (survival.getCircle() != null){
			System.out.println("Cerchio");
			g2d.drawArc(player.getPosX() - survival.getCircle().getRange () + player.getData().getX() / 2, player.getPosY() - survival.getCircle().getRange () + player.getData().getY() / 2, survival.getCircle().getRange () * 2, survival.getCircle().getRange() * 2, 0, 360);
			if (survival.getCircle().getDelay() + survival.getCircle().getBeforeCircle() <= System.currentTimeMillis())
				survival.null_circle();
		}
		
		g2d.drawImage(loader.getPlayer (player.getImagePlayer ()), player.getPosX(), player.getPosY(), this);
		if (player.getPower())
		{
			PowerUP p = player.getPowerUp();
			if (p instanceof SuperShield)
			{
				if (player.getBarchetta())
				{
					g2d.drawImage(loader.getSuperShield(), ((SuperShield) p).getPosX(), ((SuperShield) p).getPosY(), this);  
				}
				else
				{
					g2d.drawImage(loader.getBigSuperShield(), ((SuperShield) p).getPosX(), ((SuperShield) p).getPosY(), this);  
				}
			}
			
			if (p instanceof SuperSpeed)
			{
				g2d.drawImage(loader.getSuperSpeed(((SuperSpeed) p).getImage()), ((SuperSpeed) p).getPosX(), ((SuperSpeed) p).getPosY(), this);  
			}
			
			if (p instanceof AmmoPerTwo)
			{
				g2d.drawImage(loader.getAmmo(), ((AmmoPerTwo) p).getPosX(), ((AmmoPerTwo) p).getPosY(), this);  
			}
		}

		
		
		for (Enemy tmp : survival.enemies)
		{
			if (tmp != null)
			{
				if (tmp instanceof Stormtrooper)
				{
					g2d.drawImage((loader.getStormtrooper(((Stormtrooper) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
				
				if (tmp instanceof Schooner)
				{
					g2d.drawImage((loader.getSchooner(((Schooner) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
				
				if (tmp instanceof Inquisitor)
				{
					g2d.drawImage((loader.getInquisitor(((Inquisitor) tmp).getImage())), tmp.getCoordSX().getX(), tmp.getCoordSX().getY(), this);
				}
			}
				
		}
		
		for (Iterator <Explosion> it = survival.explosions.iterator(); it.hasNext();) {
			Explosion explosion = (Explosion) it.next();
			int index = explosion.getIndex();
			if (index < 25){
				g2d.drawImage(loader.explosion.get(index), explosion.getCoord().getX() - 32, explosion.getCoord().getY() - 32, this);
				explosion.updateIndex();
			}
			else
				it.remove();
		}
		
		for (int i = 0; i < 5 ; i++)
			g2d.drawImage(loader.getIsland(), survival.islands [i].getCoordSX().getX(), survival.islands[i].getCoordSX().getY(), this);
		
		if (survival.animationFire != null){
			g2d.drawImage(loader.getAnimationFire(survival.animationFire.isNumber()), player.getPosX() + (player.getData().getX() / 2 - 25), player.getPosY() - 55, this);
			if (survival.animationFire.getBefore() + survival.animationFire.getDelay() <= System.currentTimeMillis())
				survival.animationFire = null;
		}
		
		g2d.drawImage(loader.getTools(), 150, 18, this);
		String q = ""+player.getHealthPoint();
		g2d.drawString(q, 180, 35);
		
		g2d.drawImage((loader.getFlipCoin(ac.getIndex())), 10, 10, this);
		
		String s = ""+player.getDobloni();
		g2d.drawString(s, 40, 35);
		
		g2d.drawImage(loader.getAnimationAmmo(), 70, 10, this);
		String w = ""+player.getAmmo();
		g2d.drawString(w, 110, 35);
		
		Toolkit.getDefaultToolkit().sync();// sincronizzo
		g.dispose(); // dispone
	}

	public Loader getLoader() {
		return loader;
	}

	long before = System.currentTimeMillis(), delay = 10000;
	@Override
	public void run() {
		PirateTimer pt = new PirateTimer ();
		
		TimerTask tt = new TimerTask() {
			
			@Override
			public void run() 
			{
				if (!survival.checkWin())
				{
					if (!Info.Information.popUpExit && player.getHealthPoint() > 0)
					{
						pue = null;
						survival.check();
						survival.refresh();
						repaint();
					}
					else
					{
						if ( pue == null && player.getHealthPoint()>0)
						{
							pue = new PopUpExit(getLoader(), 0, survival);
						}
						else if (player.getHealthPoint() <= 0 && pue == null)
						{
							pue = new PopUpExit(getLoader(), 1, survival);
						}
					
					}
				
				}
				else 
				{
					if (pue == null)
						pue = new PopUpExit(getLoader(), 2, survival);
				}
		
			}
		};
		pt.schedule(tt, 90, 90);
		
	}
		
	
	Game loadGame (){
		return Info.Information.loadGame();
	}
	
	public class Mouse_Adapter extends MouseAdapter{
		@Override
		public void mouseClicked(MouseEvent e) {
			super.mouseClicked(e);
			int ret = player.fire(e.getX(), e.getY(), survival.enemies);
			switch (ret){
			case 0:
				survival.explosions.add(new Explosion (e.getX(), e.getY()));
				Information.audio.playSparo();
				break;
			case 1:
				if (survival.animationFire == null)
					survival.animationFire = new AnimationFire (true);
				break;
			case 2: 
				survival.splash.add(new Splash (e.getX(), e.getY()));
				Information.audio.playSplash();
				break;
			case 3:
				if (survival.animationFire == null)
					survival.animationFire = new AnimationFire(false);
				break;
			case 4:
				survival.red_circle();
				Information.audio.playSplash();
				break;
			default:
				break;
			}
		}
}
	
	private class Key_Adapter extends KeyAdapter{
		@Override
		public void keyPressed(KeyEvent arg0) {
			
			int key = arg0.getKeyCode();
			
			if (key == KeyEvent.VK_ESCAPE)
				Info.Information.popUpExit = true;
			
			super.keyPressed(arg0);
			player.check_position(survival);
			player.keyPressed(arg0);
		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			super.keyReleased(arg0);
			player.keyReleased(arg0);
		}
	
	}
}