package Graphics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JPanel;
import application.Info;
import application.Main;
import application.MainMenu;

public class LoseJPanel extends JPanel {
	
		Loader loader;
		JButton back = null;
		JButton quit = null;
		PopUpExit frame;

		public LoseJPanel(Loader _loader, PopUpExit _frame)
		{
			super();
			setFocusable(true);
			setDoubleBuffered(true);
			loader = _loader;
			back = new JButton("Back to Menu");
			quit = new JButton("Quit Game");
			this.setLayout(null);
			back.setBounds(225 ,200 , 150, 50);
			quit.setBounds(25, 200, 150, 50);
			back.setVisible(true);
			quit.setVisible(true);
			add(back);
			add(quit);
			quit.addActionListener(new Button_listener());
			back.addActionListener(new Button_listener());
			setVisible(true);
			repaint();
			frame = _frame;
		}
		
		@Override
		protected void paintComponent(Graphics g) 
		{		
			super.paintComponent(g);
			Graphics2D g2d = (Graphics2D)g;
			g2d.drawImage(loader.getLoser(), 0, 0, this);

			back.repaint();
			quit.repaint();
			Toolkit.getDefaultToolkit().sync();// sincronizzo
			g.dispose(); // dispone
			
		}

		class Button_listener implements ActionListener {
			  public void actionPerformed(ActionEvent e) {
			    		    JButton button = (JButton)e.getSource();
			    if (button.equals(quit))
			    {
			    	System.exit(1);
			    }
			    if (button.equals(back))
			    {
			    	MainMenu m = new MainMenu(Info.Information.info);
			    	m.showMainMenu();
			    }
			  }
			}
}

