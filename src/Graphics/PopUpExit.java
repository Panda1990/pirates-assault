package Graphics;import javax.swing.JFrame;

import Pirates_Assault.Game;

public class PopUpExit extends JFrame {
	
	private static final long serialVersionUID = 1L;
	int width; 
	int height; 
	Game game;	PirateFrame pf = null;
	
	public PopUpExit(Loader _loader, int _choosePanel, Game _game) 	{		super();		width = 400;
		height = 400;
		setSize(width, height);
		setUndecorated(true);
		setLocationRelativeTo(null);// si posiziona al centro dello schermo
		setResizable(false);
		getContentPane().removeAll();
		invalidate();
		if (_choosePanel == 0)
		{
			ExitJPanel ep = new ExitJPanel(_loader, this);
			this.getContentPane().add(ep);
			this.getContentPane().validate();
			this.getContentPane().repaint();
			setVisible(true);
		}
		if (_choosePanel == 1)
		{
			LoseJPanel lp = new LoseJPanel(_loader, this);
			this.getContentPane().add(lp);
			this.getContentPane().validate();
			this.getContentPane().repaint();
			setVisible(true);
		}		if (_choosePanel == 2)		{			WinJPanel lp = new WinJPanel(_loader, this);			this.getContentPane().add(lp);			this.getContentPane().validate();			this.getContentPane().repaint();			setVisible(true);		}
		game = _game;	}
	
	public void setVisibility(boolean b)
	{		this.setVisible(b);
	}
}
