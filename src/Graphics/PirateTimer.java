package Graphics;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class PirateTimer extends Timer {
	
	@Override
	public void schedule(TimerTask task, long delay, long period) {
		super.schedule(task, delay, period);
		//task.run();
	}
	
	@Override
	public void schedule(TimerTask task, Date time) {
		//super.schedule(task, time);
		task.run();
	}
	
	
}
