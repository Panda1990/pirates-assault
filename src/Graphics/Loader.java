package Graphics;

import java.awt.Image;
import java.awt.Toolkit;
import java.util.ArrayList;
import java.util.List;

public class Loader {
	
	Toolkit tk = Toolkit.getDefaultToolkit();
	
	List <Image> stormtrooper;
	List <Image> explosion;
	List <Image> destroyed;
	List <Image> player;
	List <Image> schooner;
	List <Image> inquisitor;
	List <Image> fish;
	List <Image> trunk;
	List <Image> bottle;
	List <Image> barrel;
	List <Image> super_speed;
	List <Image> flip_coin;
	List <Image> splash;
	Image first;
	Image second;
	Image win = null;
	Image super_shield = null;
	Image tools = null;
	Image pop_up = null;
	Image loser = null;
	Image [][] sea;
	Image island;
	Image ammo = null;
	Image animationAmmo = null;
	Image big_super_shield = null;
	
	public Image getPlayer (int sprite){
		return player.get(sprite);
	}
	
	public void loadPlayer (){
		player = new ArrayList<Image>(13);
		
		player.add(tk.getImage("images/barchetta_pirata/barca_vela_triangolo_posteriore_1.png"));
		player.add(tk.getImage("images/barchetta_pirata/barca_vela_triangolo_posteriore_2.png"));
		player.add(tk.getImage("images/barchetta_pirata/barchetta_pirata_1.png"));
		player.add(tk.getImage("images/barchetta_pirata/barchetta_pirata_left.png"));
		player.add(tk.getImage("images/barchetta_pirata/barchetta_pirata_down.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_posteriore_1.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_posteriore_2.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_destra_1.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_destra_2.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_sinistra_1.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_sinistra_2.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_anteriore_1.png"));
		player.add(tk.getImage("images/inquisitore_pirata_notte/inquisitore_notte_pirata_anteriore_2.png"));
	}
	
	public void loadIsland (){
		island = tk.getImage("images/Island.png");
	}
	
	public Image getIsland (){
		return island;
	}
	
	public void loadSea (){
			sea = new Image [2][2];
			sea [0][0] = tk.getImage("images/sea.jpg");
			sea [0][1] = tk.getImage("images/sea2.jpg");
			sea [1][0] = tk.getImage("images/sea3.jpg");
			sea [1][1] = tk.getImage("images/sea4.jpg");
	}
	
	public Image getSea (int _indexI, int _indexJ){
		if ((_indexI & 1) == 0){
			if ((_indexJ & 1) == 0)
				return sea [0][0];
			else
				return sea [1][0];
		}
		else{
			if ((_indexJ & 1) == 0)
				return sea [0][1];
			else
				return sea [1][1];
		}
			
		
	}
	
	public void loadFlipCoin()
	{
		flip_coin = new ArrayList<Image>(4);
		flip_coin.add(tk.getImage("images/doblone/doblone_1.png"));
		flip_coin.add(tk.getImage("images/doblone/doblone_4.png"));
		flip_coin.add(tk.getImage("images/doblone/doblone_3.png"));
		flip_coin.add(tk.getImage("images/doblone/doblone_2.png"));
	}
	
	public Image getFlipCoin(int index)
	{
		return flip_coin.get(index);
	}
	
	public void loadAnimationAmmo()
	{
		animationAmmo = tk.getImage("images/animationAmmo/bomba_rossa.png");
	}
	
	public Image getAnimationAmmo()
	{
		return animationAmmo;
	}
	
	public void loadTools()
	{
		tools = tk.getImage("images/arnesi/arnesi.png");
	}
	
	public Image getTools()
	{
		return tools;
	}
	
	public void loadLoser()
	{
		loser = tk.getImage("images/pop_up/loser.png");
	}
	
	public Image getLoser()
	{
		return loser;
	}
	
	public void loadWin()
	{
		win = tk.getImage("images/pop_up/vittoria.png");
	}
	
	public Image getWin()
	{
		return win;
	}
	
	public Image getBigSuperShield()
	{
		return big_super_shield;
	}
	
	public void loadBigSuperShield()
	{
		big_super_shield = tk.getImage("images/powerup/cerchio_di_fuoco_grande.png");
	}
	
	public void loadAmmo()
	{
		ammo = tk.getImage("images/ammo/ammox2.png");
	}
	
	public Image getAmmo()
	{
		return ammo;
	}
	
	public void loadSuperSpeed()
	{
		super_speed = new ArrayList<Image>(4);
		super_speed.add(tk.getImage("images/thunder/thunder_right.png"));
		super_speed.add(tk.getImage("images/thunder/thunder_left.png"));
		super_speed.add(tk.getImage("images/thunder/thunder_up.png"));
		super_speed.add(tk.getImage("images/thunder/thunder_down.png"));
	}
	
	public Image getSuperSpeed(int index)
	{
		return super_speed.get(index);
	}
	
	public void loadStormtrooper (){
		stormtrooper = new ArrayList <Image> (8);
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_destra_1.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_destra_2.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_sinistra_1.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_sinistra_2.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_anteriore_1.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_anteriore_2.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_posteriore_1.png"));
		stormtrooper.add(tk.getImage("images/assaltatrice/assaltatrice_posteriore_2.png"));
	}
	
	public Image getStormtrooper (int index){
		return stormtrooper.get(index);
	}
	
	public void loadPopUp()
	{
		pop_up = tk.getImage("images/pop_up/pop_up.png");
	}
	
	public Image getPopUp()
	{
		return pop_up;
	}
	
	public void loadBottle()
	{
		bottle = new ArrayList<Image>(2);
		bottle.add(tk.getImage("images/bottle/bottle_left_short.png"));
		bottle.add(tk.getImage("images/bottle/bottle_right_short.png"));
	}
	
	public void loadSuperShield()
	{
		super_shield = Toolkit.getDefaultToolkit().getImage("images/powerup/cerchio_di_fuoco.png");
	}
	
	public Image getSuperShield()
	{
		return super_shield;
	}
	
	public void loadBarrel ()
	{
		barrel = new ArrayList<Image> (2);
		barrel.add(Toolkit.getDefaultToolkit().getImage("images/barile/barile_intero_2_sinistra.png"));
		barrel.add(Toolkit.getDefaultToolkit().getImage("images/barile/barile_intero_1_destra"));
	}
	
	public Image getBarrel (int index)
	{
		return barrel.get(index);
	}
	
	public Image getBottle(int index)
	{
		return bottle.get(index);
	}
	
	public void loadTrunk()
	{
		trunk = new ArrayList<Image> (2);
		trunk.add(tk.getImage("images/trunk/cassa_sinistra.png"));
		trunk.add(tk.getImage("images/trunk/cassa_destra.png"));
	}
	
	public Image getTrunk(int index)
	{
		return trunk.get(index);
	}
	
	public void loadFish()
	{
		fish = new ArrayList<Image>(22);
		fish.add(tk.getImage("images/pesci/carpa.png"));
		fish.add(tk.getImage("images/pesci/pesce_arancione.png"));
		fish.add(tk.getImage("images/pesci/pesce_arancione_e_verde.png"));
		fish.add(tk.getImage("images/pesci/pesce_blu.png"));
		fish.add(tk.getImage("images/pesci/pesce_giallo.png"));
		fish.add(tk.getImage("images/pesci/pesce_giallo_2.png"));
		fish.add(tk.getImage("images/pesci/pesce_pagliaccio.png"));
		fish.add(tk.getImage("images/pesci/pesce_tigre.png"));
		fish.add(tk.getImage("images/pesci/pesce_tigre_verde.png"));
		fish.add(tk.getImage("images/pesci/pesce_verde.png"));
		fish.add(tk.getImage("images/pesci/pesce_viola.png"));
		fish.add(tk.getImage("images/pesci/pesce_violetto_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_viola_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_verde_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_tigre_verde_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_tigre_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_rosa_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_pagliaccio_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_giallo_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_arancione_sinistra.png"));
		fish.add(tk.getImage("images/pesci/pesce_arancione_e_verde_sinistra.png"));
		fish.add(tk.getImage("images/pesci/carpa_sinistra.png"));
	}
	
	public Image getFish(int index)
	{
		return fish.get(index);
	}
	
	public void loadSchooner ()
	{
		schooner = new ArrayList <Image>(8);
		schooner.add(tk.getImage("images/goletta/goletta_destra_1.png"));
		schooner.add(tk.getImage("images/goletta/goletta_destra_2.png"));
		schooner.add(tk.getImage("images/goletta/goletta_sinistra_1.png"));
		schooner.add(tk.getImage("images/goletta/goletta_sinistra_2.png"));
		schooner.add(tk.getImage("images/goletta/goletta_anteriore_1.png"));
		schooner.add(tk.getImage("images/goletta/goletta_anteriore_2.png"));
		schooner.add(tk.getImage("images/goletta/goletta_posteriore_1.png"));
		schooner.add(tk.getImage("images/goletta/goletta_posteriore_2.png"));
	}
	
	public Image getSchooner (int index)
	{
		return schooner.get(index);
	}
	
	public void loadInquisitor ()
	{
		inquisitor = new ArrayList <Image>(8);
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_destra_1.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_destra_2.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_sinistra_1.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_sinistra_2.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_anteriore_1.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_anteriore_2.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_posteriore_1.png"));
		inquisitor.add(tk.getImage("images/inquisitore/inquisitore_posteriore_2.png"));
	}
	
	public Image getInquisitor (int index)
	{
		return inquisitor.get(index);
	}
	
	public void loadExplosion (){
		explosion = new ArrayList<Image>(25);
		explosion.add(tk.getImage("images/Explosion/explosion1.png"));
		explosion.add(tk.getImage("images/Explosion/explosion2.png"));
		explosion.add(tk.getImage("images/Explosion/explosion3.png"));
		explosion.add(tk.getImage("images/Explosion/explosion4.png"));
		explosion.add(tk.getImage("images/Explosion/explosion5.png"));
		explosion.add(tk.getImage("images/Explosion/explosion6.png"));
		explosion.add(tk.getImage("images/Explosion/explosion7.png"));
		explosion.add(tk.getImage("images/Explosion/explosion8.png"));
		explosion.add(tk.getImage("images/Explosion/explosion9.png"));
		explosion.add(tk.getImage("images/Explosion/explosion10.png"));
		explosion.add(tk.getImage("images/Explosion/explosion11.png"));
		explosion.add(tk.getImage("images/Explosion/explosion12.png"));
		explosion.add(tk.getImage("images/Explosion/explosion13.png"));
		explosion.add(tk.getImage("images/Explosion/explosion14.png"));
		explosion.add(tk.getImage("images/Explosion/explosion15.png"));
		explosion.add(tk.getImage("images/Explosion/explosion16.png"));
		explosion.add(tk.getImage("images/Explosion/explosion17.png"));
		explosion.add(tk.getImage("images/Explosion/explosion18.png"));
		explosion.add(tk.getImage("images/Explosion/explosion19.png"));
		explosion.add(tk.getImage("images/Explosion/explosion20.png"));
		explosion.add(tk.getImage("images/Explosion/explosion21.png"));
		explosion.add(tk.getImage("images/Explosion/explosion22.png"));
		explosion.add(tk.getImage("images/Explosion/explosion23.png"));
		explosion.add(tk.getImage("images/Explosion/explosion24.png"));
		explosion.add(tk.getImage("images/Explosion/explosion25.png"));
	}
	
	public void loadDestroyed (){
		destroyed = new ArrayList<Image>(5);
		destroyed.add(tk.getImage("images/Destroyed/Boat.png"));
		destroyed.add(tk.getImage("images/Destroyed/doblone_1.png"));
		destroyed.add(tk.getImage("images/Destroyed/doblone_4.png"));
		destroyed.add(tk.getImage("images/Destroyed/doblone_3.png"));
		destroyed.add(tk.getImage("images/Destroyed/doblone_2.png"));
	}
	
	public Image getDestroyed (int index){
		return destroyed.get(index);
	}
	
	public void loadSplash (){
		splash = new ArrayList<Image>(2);
		splash.add(tk.getImage("images/Splash/splash1.png"));
		splash.add(tk.getImage("images/Splash/splash2.png"));
	}
	
	public Image getSplash (int index){
		return splash.get(index);
	}
	
	public void loadAnimationFire (){
		first = tk.getImage("images/noMunition.png");
		second = tk.getImage("images/timer.png");
	}
	
	public Image getAnimationFire(boolean number){
		if (number)
			return first;
		return second;
	}
	
	public void loadGame (){
		loadStormtrooper();
		loadExplosion();
		loadSchooner();
		loadInquisitor();
		loadSea();
		loadFish();
		loadTrunk();
		loadBottle();
		loadBarrel();
		loadSuperShield();
		loadIsland();
		loadSuperSpeed();
		loadAmmo();
		loadFlipCoin();
		loadPlayer();
		loadAnimationAmmo();
		loadBigSuperShield();
		loadTools();
		loadDestroyed();
		loadSplash();
		loadAnimationFire();
		loadPopUp();
		loadLoser();
		loadWin();
	}

}
