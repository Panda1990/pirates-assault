package Graphics;

import java.awt.Toolkit;
import javax.swing.JFrame;

import PirateOnline.MultiPlayerJPanel;
import application.Info;

public class PirateFrame extends JFrame {
	
	private static final long serialVersionUID = 1L;

	public PirateFrame (int state, int intelligent, boolean choose) 
	{
		super();
		int width = (int) Toolkit.getDefaultToolkit().getScreenSize().getWidth();//larghezza
		int height = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();//altezza
		setSize(width, height);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);// si posiziona al centro dello schermo
		setResizable(true);
		setUndecorated(false);
		switch (state){
		case 0:
			add(new GameJPanel(intelligent, choose));
			break;
		case 1:
			add(new SurvivalJPanel(intelligent));
			break;
		case 2:
			add(new MultiPlayerJPanel());
			break;
		default:
			System.out.println("ERROR !!!");
			break;
		}
		setVisible(true);
	}

}
